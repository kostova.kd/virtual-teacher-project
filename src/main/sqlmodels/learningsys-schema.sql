create schema learningsys collate utf8_general_ci;

create table topics
(
	topicid int unsigned auto_increment
		primary key,
	topic varchar(50) not null
);

create table users
(
	userid int unsigned auto_increment
		primary key,
	username varchar(50) not null,
	password varchar(100) not null,
	mail varchar(50) not null,
	picture varchar(200) null,
	enabled tinyint not null
);

create table assignments
(
	assignmentid int unsigned auto_increment
		primary key,
	title varchar(50) not null,
	filepath varchar(200) not null,
	authorid int unsigned null,
	constraint assignments_users_userid_fk
		foreign key (authorid) references users (userid)
);

create table authorities
(
	userid int unsigned not null,
	authority varchar(50) not null,
	primary key (userid, authority),
	constraint authorities_users_userid_fk
		foreign key (userid) references users (userid)
);

create table courses
(
	courseid int unsigned auto_increment
		primary key,
	title varchar(50) not null,
	description text not null,
	program text not null,
	imagepath varchar(200) not null,
	rating double null,
	userid int unsigned null,
	topicid int unsigned not null,
	constraint courses_topics_topicid_fk
		foreign key (topicid) references topics (topicid),
	constraint courses_users_userid_fk
		foreign key (userid) references users (userid)
);

create table ratings
(
	ratingid int unsigned auto_increment
		primary key,
	rating int unsigned not null,
	userid int unsigned not null,
	courseid int unsigned not null,
	constraint ratings_courses_courseid_fk
		foreign key (courseid) references courses (courseid),
	constraint ratings_users_userid_fk
		foreign key (userid) references users (userid)
);

create table user_courses
(
	user_course_id int unsigned auto_increment
		primary key,
	userid int unsigned not null,
	courseid int unsigned not null,
	status varchar(50) not null,
	average_grade double null,
	constraint user_courses_courses_courseid_fk
		foreign key (courseid) references courses (courseid),
	constraint user_courses_users_userid_fk
		foreign key (userid) references users (userid)
);

create table solutions
(
	solutionid int unsigned auto_increment
		primary key,
	filepath varchar(50) not null,
	assignmentid int unsigned not null,
	userid int unsigned not null,
	grade int(10) null,
	usercourseid int unsigned null,
	constraint solutions_assignments_assignmentid_fk
		foreign key (assignmentid) references assignments (assignmentid),
	constraint solutions_user_courses_user_course_id_fk
		foreign key (usercourseid) references user_courses (user_course_id),
	constraint solutions_users_userid_fk
		foreign key (userid) references users (userid)
);

create table videos
(
	videoid int unsigned auto_increment
		primary key,
	filepath varchar(50) not null
);

create table lectures
(
	lectureid int unsigned auto_increment
		primary key,
	title varchar(50) not null,
	description text not null,
	imagepath varchar(200) not null,
	assignmentid int unsigned not null,
	videoid int unsigned not null,
	courseid int unsigned not null,
	constraint lectures_assignments_assignmentid_fk
		foreign key (assignmentid) references assignments (assignmentid),
	constraint lectures_courses_courseid_fk
		foreign key (courseid) references courses (courseid),
	constraint lectures_videos_videoid_fk
		foreign key (videoid) references videos (videoid)
);