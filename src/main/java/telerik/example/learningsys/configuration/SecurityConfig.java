package telerik.example.learningsys.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.JdbcUserDetailsManager;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import javax.sql.DataSource;

@Configuration
@EnableWebSecurity
@PropertySource("application.properties")
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    private String dbUrl, dbUsername, dbPassword;

    @Autowired
    public SecurityConfig(Environment env) {
        dbUrl = env.getProperty("database.url");
        dbUsername = env.getProperty("database.username");
        dbPassword = env.getProperty("database.password");
    }

    @Bean("authenticationManager")
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Bean
    public AuthenticationSuccessHandler myAuthenticationSuccessHandler() {
        return new AuthenticationSuccessHandlerImpl();
    }

    // Configuration which connects a User with their Role.
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.jdbcAuthentication()
                .dataSource(securityDataSource())
                .passwordEncoder(passwordEncoder())
                .authoritiesByUsernameQuery("select u.username, a.authority, u.enabled from authorities a join users u on u.userid = a.userid where username = ?")
                .usersByUsernameQuery("SELECT username, password, enabled from users where username = ?");
    }

    // Configuration which handles HTTP traffic.
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // Authorizes which resources Everyone, User and Admin can access.
        http.authorizeRequests()
                .antMatchers("/courses/new").hasRole("TEACHER")
                .antMatchers("/courses/{id}/edit").hasRole("TEACHER")
                .antMatchers("/solutions").hasRole("TEACHER")
                .antMatchers("/home").permitAll()
                .antMatchers("/courses/**").permitAll()
                .antMatchers("/users/**").authenticated()
                .antMatchers("/users").hasRole("ADMIN")
                .antMatchers("/users/new").hasRole("ADMIN")
                .antMatchers("/editor/**").authenticated()
                .antMatchers("/removed-users/**").hasRole("ADMIN")
                .antMatchers("/password-editor/**").authenticated()
                .antMatchers("/**").permitAll()
                .antMatchers("/home/**").permitAll()
                .antMatchers("/upload/**").permitAll()
                .and()
                .formLogin()
                    .loginPage("/login")
                    .loginProcessingUrl("/authenticate") // Comes from Spring Security, when we authenticate.
                    .successHandler(myAuthenticationSuccessHandler())
                .permitAll()
                .and()
                .logout()
                .permitAll();

        // Enable Cross-Origin Resource Sharing and disable Cross-Site Request Forgery.
        // We can do Postman requests with this configuration.
        http.cors().and().csrf().disable();
    }

    @Bean
    public DataSource securityDataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("com.mysql.cj.jdbc.Driver");
        dataSource.setUrl(dbUrl);
        dataSource.setUsername(dbUsername);
        dataSource.setPassword(dbPassword);
        return dataSource;
    }

    @Bean
    public UserDetailsManager userDetailsManager() {
        JdbcUserDetailsManager jdbcUserDetailsManager = new JdbcUserDetailsManager();
        jdbcUserDetailsManager.setDataSource(securityDataSource());
        return jdbcUserDetailsManager;
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }


}
