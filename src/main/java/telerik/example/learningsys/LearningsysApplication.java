package telerik.example.learningsys;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LearningsysApplication {

    public static void main(String[] args) {
        SpringApplication.run(LearningsysApplication.class, args);
    }

}
