package telerik.example.learningsys.services.Interfaces;

import telerik.example.learningsys.models.Assignment;
import telerik.example.learningsys.models.Lecture;
import telerik.example.learningsys.models.Video;

import java.util.List;

public interface LectureService {

    List<Lecture> getAll();
    Lecture getLectureById(int id);
    void create(Lecture lecture);
    void update(int lectureId, Lecture lecture);
    void delete(int lectureId);
    void addCourseLecture(int id, Lecture lecture, Video video, Assignment assignment);
}
