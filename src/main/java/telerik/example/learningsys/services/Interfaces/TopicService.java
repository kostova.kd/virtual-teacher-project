package telerik.example.learningsys.services.Interfaces;

import telerik.example.learningsys.models.Topic;

import java.util.List;

public interface TopicService {

    List<Topic> getAll();
    void create(Topic topic);
    void update(int topicId, Topic topic);
    void delete(int topicId);
}
