package telerik.example.learningsys.services.Interfaces;

import telerik.example.learningsys.models.Solution;

import java.util.List;

public interface SolutionService {

    List<Solution> getAll();
    List<Solution> getSolutionsByAssignmentAndUserId(int authorId);
    Solution getById(int id);
    List<Solution> getSolutionsByUserCourseId(int userCourseId);
    void create(Solution solution);
    void update(int solutionId, Solution solution);
    void delete(int solutionId);
}
