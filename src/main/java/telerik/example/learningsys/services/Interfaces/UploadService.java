package telerik.example.learningsys.services.Interfaces;

import org.springframework.web.multipart.MultipartFile;
import telerik.example.learningsys.models.Assignment;
import telerik.example.learningsys.models.Lecture;
import telerik.example.learningsys.models.Video;

import java.security.Principal;

public interface UploadService {

    String writeFile(Principal principal, MultipartFile file, String parentDir, String uploadDir) throws java.io.IOException;
    void uploadSolution(Principal principal, MultipartFile file, int assignmentId);
    Video uploadVideo(Principal principal, MultipartFile file, int lectureId);
    Assignment uploadAssignment(Principal principal, MultipartFile file, int lectureId, String title);
    Lecture addImageToLecture(Principal principal, MultipartFile file, Lecture lecture);
}
