package telerik.example.learningsys.services.Interfaces;

import telerik.example.learningsys.models.Course;
import telerik.example.learningsys.models.Lecture;
import telerik.example.learningsys.models.Solution;
import telerik.example.learningsys.models.UserCourse;

import java.util.List;

public interface UserCourseService {
    UserCourse getUserCourseByUserAndCourseId(int userId, int courseId);
    List<UserCourse> getUserEnrolledCourses(int userId);
    List<UserCourse> getUserCompletedCourses(int userId);
    boolean userCourseIsCompleted(UserCourse userCourse);
    List<Course> getTeacherDraftCourses(int userId);
    List<Course> getTeacherReadyCourses(int userId);
    UserCourse getUserCourseById(int userCourseId);
    void updateUserCourse(UserCourse userCourse);
    void setAverageGrade(int userCourseId);

    List<Solution> getUserCourseCompletedSolutions(UserCourse userCourse);
    List<Lecture> getUserCourseInProgressLectures(UserCourse userCourse);

    String getAverageGrade(UserCourse userCourse);

    Integer getProgress(UserCourse userCourse);
}
