package telerik.example.learningsys.services.Interfaces;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;
import telerik.example.learningsys.models.Course;
import telerik.example.learningsys.models.User;

import java.security.Principal;
import java.util.List;

public interface UserService {

    List<User> listUsers();
    User getUserByName(String name);
    User getUserById(int id);
    String getUserAuthorityById(int id);
    void createUser(User user, String authority);
    void updateUser(int id, User user, String authority);
    void updatePassword(int id, String password);
    void deleteUser(int id);

    void enrollToCourse(int userId, int courseId);
    boolean userEnrolledToCourse(int userId, int courseId);
    void completeCourse(int userId, int courseId, String status);
    Page<User> findPaginatedUsers(Pageable pageable);

    void addRating(int courseid, Principal principal, int rating);
}
