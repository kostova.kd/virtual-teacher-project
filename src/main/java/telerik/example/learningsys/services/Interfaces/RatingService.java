package telerik.example.learningsys.services.Interfaces;

import telerik.example.learningsys.models.Rating;

import java.util.List;

public interface RatingService {

    List<Rating> getAll();
    List<Rating> getRatingsOfCourse(int courseid);
    void create(Rating rating);
    void update(int ratingId, Rating rating);
    void delete(int ratingId);
}
