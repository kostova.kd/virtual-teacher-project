package telerik.example.learningsys.services.Interfaces;

import telerik.example.learningsys.models.Assignment;

import java.util.List;

public interface AssignmentService {
    List<Assignment> getAll();
    Assignment getAssignmentById(int id);
    void create(Assignment assignment);
    void update(int assignmentId, Assignment assignment);
    void delete(int assignmentId);
}
