package telerik.example.learningsys.services.Interfaces;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import telerik.example.learningsys.models.Course;
import telerik.example.learningsys.models.Lecture;

import java.util.List;

public interface CourseService {
    List<Course> getAll();
    Course getCourseById(int id);
    List<Lecture> getCourseLectures(int courseId);
    void create(Course course);
    void update(int courseId, Course course);
    void delete(int courseId);
    double getAverageRating(int courseId);
}
