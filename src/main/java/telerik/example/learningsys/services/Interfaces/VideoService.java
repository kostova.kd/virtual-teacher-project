package telerik.example.learningsys.services.Interfaces;

import telerik.example.learningsys.models.Video;

import java.util.List;

public interface VideoService {

    List<Video> getAll();
    void create(Video video);
    void update(int videoId, Video video);
    void delete(int topicId);
}
