package telerik.example.learningsys.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import telerik.example.learningsys.models.*;
import telerik.example.learningsys.repositories.interfaces.UserCourseRepository;
import telerik.example.learningsys.repositories.interfaces.UserRepository;
import telerik.example.learningsys.services.Interfaces.UserCourseService;

import java.text.DecimalFormat;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserCourseServiceImpl implements UserCourseService {

    private SolutionServiceImpl solutionService;
    private UserCourseRepository userCourseRepository;
    private UserRepository userRepository;

    @Autowired
    public UserCourseServiceImpl(UserCourseRepository userCourseRepository, UserRepository userRepository,
                                 SolutionServiceImpl solutionService) {
        this.userCourseRepository = userCourseRepository;
        this.userRepository = userRepository;
        this.solutionService = solutionService;
    }

    @Override
    public UserCourse getUserCourseById(int userCourseId) {
        UserCourse userCourse = userCourseRepository.getUserCourseById(userCourseId);
        if (userCourse == null) {
            throw new IllegalArgumentException(String.format("UserCourse with id %d not found.", userCourseId));
        }
        return userCourse;
    }

    @Override
    public void updateUserCourse(UserCourse userCourse) {
        if (userCourse == null) {
            throw new IllegalArgumentException("Missing data in one or more fields!");
        }
        if (userCourse.getCourse() == null || userCourse.getUser() == null || userCourse.getStatus() == null) {
            throw new IllegalArgumentException("Missing data in one or more fields!");
        }

        userCourseRepository.updateUserCourse(userCourse);
    }

    @Override
    public UserCourse getUserCourseByUserAndCourseId(int userId, int courseId) {
        return userCourseRepository.getUserCourseByUserAndCourseId(userId, courseId);
    }

    @Override
    public List<UserCourse> getUserEnrolledCourses(int userId) {
        return userCourseRepository.getUserCoursesByStatus(userId, "enrolled");
    }

    @Override
    public List<UserCourse> getUserCompletedCourses(int userId) {
        return userCourseRepository.getUserCoursesByStatus(userId,"completed");
    }

    @Override
    public boolean userCourseIsCompleted(UserCourse userCourse) {
        if (userCourse == null) {
            return false;
        }

        if (userCourse.getStatus().equals("completed")) {
            return true;
        }

        List<Solution> gradedSolutions = getUserCourseCompletedSolutions(userCourse);
        List<Lecture> allCourseLectures = userCourse.getCourse().getLectures();

        if (gradedSolutions.size() == allCourseLectures.size()) {
            userCourse.setStatus("completed");
            userCourseRepository.updateUserCourse(userCourse);
            return true;
        }
        return false;
    }

    @Override
    public List<Course> getTeacherDraftCourses(int userId) {
        User user = userRepository.getUserById(userId);
        List<Course> draftCourses = user.getTeacherCourses()
                .stream()
                .filter(course -> course.getLectures().isEmpty())
                .collect(Collectors.toList());
        return draftCourses;
    }

    @Override
    public List<Course> getTeacherReadyCourses(int userId) {
        User user = userRepository.getUserById(userId);
        List<Course> readyCourses = user.getTeacherCourses()
                .stream()
                .filter(course -> !course.getLectures().isEmpty())
                .collect(Collectors.toList());
        return readyCourses;
    }

    @Override
    public void setAverageGrade(int userCourseId) {
        double avgGrade = solutionService.getSolutionsByUserCourseId(userCourseId).stream()
                .map(Solution::getGrade)
                .mapToDouble(Integer::doubleValue)
                .average().orElse(0);
        UserCourse userCourse = getUserCourseById(userCourseId);
        userCourse.setAverageGrade(avgGrade);
        userCourseRepository.updateUserCourse(userCourse);
    }

    @Override
    public List<Solution> getUserCourseCompletedSolutions(UserCourse userCourse) {
        if (userCourse != null) {
            List<Solution> completedCourseSolutions = userCourse.getSolutions()
                    .stream()
                    .filter(solution -> solution.getGrade() != -1)
                    .collect(Collectors.toList());
            return completedCourseSolutions;
        }
        return null;
    }

    @Override
    public List<Lecture> getUserCourseInProgressLectures(UserCourse userCourse) {
        if (userCourse == null) {
            return null;
        }
        List<Solution> gradedSolutions = getUserCourseCompletedSolutions(userCourse);
        List<Lecture> allCourseLectures = userCourse.getCourse().getLectures();

        if (gradedSolutions == null) {
            return allCourseLectures;
        }
        List<Lecture> completedLectures = gradedSolutions
                .stream()
                .map(solution -> solution.getAssignment().getLecture())
                .collect(Collectors.toList());

        List<Lecture> inProgressLectures = allCourseLectures
                .stream()
                .filter(lecture -> !completedLectures.contains(lecture))
                .collect(Collectors.toList());

        return inProgressLectures;
    }

    @Override
    public String getAverageGrade(UserCourse userCourse) {
        DecimalFormat f = new DecimalFormat("##.0");
        if (userCourse != null && userCourse.getAverageGrade() != -1) {
            return f.format(userCourse.getAverageGrade());
        }
        return null;
    }

    @Override
    public Integer getProgress(UserCourse userCourse) {
        if (userCourse == null) {
            return 0;
        }

        List<Solution> gradedSolutions = getUserCourseCompletedSolutions(userCourse);
        List<Lecture> allCourseLectures = userCourse.getCourse().getLectures();
        double progress = (double) gradedSolutions.size() / allCourseLectures.size();
        return (int) (progress * 100);
    }
}
