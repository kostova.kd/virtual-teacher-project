package telerik.example.learningsys.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import telerik.example.learningsys.models.Rating;
import telerik.example.learningsys.repositories.interfaces.RatingRepository;
import telerik.example.learningsys.services.Interfaces.RatingService;

import java.util.List;

@Service
public class RatingServiceImpl implements RatingService {
    RatingRepository ratingRepository;

    @Autowired
    public RatingServiceImpl(RatingRepository ratingRepository) {
        this.ratingRepository = ratingRepository;
    }

    @Override
    public List<Rating> getAll() {
        return ratingRepository.getAll();
    }

    @Override
    public List<Rating> getRatingsOfCourse(int courseid) {
        return ratingRepository.getRatingsOfCourse(courseid);
    }

    @Override
    public void create(Rating rating) {
        if (rating == null) {
            throw new IllegalArgumentException("Missing data in one or more fields!");
        }
        if (rating.getCourse()==null || rating.getAuthor()==null || rating.getRating()==0
                || rating.getRating()<1 || rating.getRating()>5) {
            throw new IllegalArgumentException("Missing or invalid data in one or more fields!");
        }
        ratingRepository.create(rating);
    }

    @Override
    public void update(int ratingId, Rating rating) {
        rating.setId(ratingId);
        ratingRepository.update(rating);
    }

    @Override
    public void delete(int ratingId) {
        Rating rating = ratingRepository.getRatingById(ratingId);
        ratingRepository.delete(rating);
    }
}
