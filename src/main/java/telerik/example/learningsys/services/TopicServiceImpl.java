package telerik.example.learningsys.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import telerik.example.learningsys.models.Topic;
import telerik.example.learningsys.repositories.interfaces.TopicRepository;
import telerik.example.learningsys.services.Interfaces.TopicService;

import java.util.List;

@Service
public class TopicServiceImpl implements TopicService {
    private TopicRepository topicRepository;

    @Autowired
    public TopicServiceImpl(TopicRepository topicRepository) {
        this.topicRepository = topicRepository;
    }

    @Override
    public List<Topic> getAll() {
        return topicRepository.getAll();
    }

    @Override
    public void create(Topic topic) {
        topicRepository.create(topic);
    }

    @Override
    public void update(int topicId, Topic topic) {
        topic.setId(topicId);
        topicRepository.update(topic);
    }

    @Override
    public void delete(int topicId) {
        Topic topic = topicRepository.getTopicById(topicId);
        topicRepository.delete(topic);
    }
}