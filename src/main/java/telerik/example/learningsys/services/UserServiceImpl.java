package telerik.example.learningsys.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import telerik.example.learningsys.models.*;
import telerik.example.learningsys.repositories.interfaces.UserRepository;
import telerik.example.learningsys.services.Interfaces.*;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.Principal;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {
    private static double MIN_AVG_GRADE = 3.5;

    private UserRepository userRepository;
    private CourseService courseService;
    private UserCourseService userCourseService;
    private RatingService ratingService;


    @Autowired
    public UserServiceImpl(UserRepository userRepository, CourseService courseService,
                           UserCourseService userCourseService, RatingService ratingService) {
        this.userRepository = userRepository;
        this.courseService = courseService;
        this.userCourseService=userCourseService;
        this.ratingService=ratingService;
    }

    @Override
    public List<User> listUsers() {
        return userRepository.listUsers();
    }

    @Override
    public User getUserByName(String name) {
        User user = userRepository.getUserByName(name);
        if (user == null) {
            throw new IllegalArgumentException(String.format("user with name %s not found.", name));
        }
        return user;
    }

    @Override
    public User getUserById(int id) {
        User user = userRepository.getUserById(id);
        if (user == null) {
            throw new IllegalArgumentException(String.format("user with id %d not found.", id));
        }
        return user;
    }

    @Override
    public String getUserAuthorityById(int id) {
        if (userRepository.getUserById(id) == null) {
            throw new IllegalArgumentException(String.format("user with id %d not found.", id));
        }
        return userRepository.getUserAuthorityById(id);
    }

    @Override
    public void createUser(User user, String authority) {
        if (user == null) {
            throw new IllegalArgumentException("Missing data in one or more fields!");
        }
        if (user.getUsername().isEmpty() || user.getMail().isEmpty() || user.getPassword().isEmpty()) {
            throw new IllegalArgumentException("Missing data in one or more fields!");
        }
        List<User> users = userRepository.listUsers().stream()
                .filter(x -> x.getUsername().equals(user.getUsername()))
                .collect(Collectors.toList());

        if (users.size() > 0) {
            throw new IllegalArgumentException(
                    String.format("User with name %s already exists", user.getUsername()));
        }
        userRepository.createUser(user, authority);
    }

    @Override
    public void updateUser(int id, User user, String authority) {
        if (user == null) {
            throw new IllegalArgumentException("Missing data in one or more fields!");
        }
        if (user.getPassword() == null || user.getPassword().isEmpty()) {
            String existingPassword = userRepository.getUserById(id).getPassword();
            user.setPassword(existingPassword);
        }
        if (user.getMail().isEmpty()) {
            throw new IllegalArgumentException("Missing data in one or more fields!");
        }

        userRepository.updateUser(id, user, authority);
    }

    @Override
    public void updatePassword(int id, String password) {
        userRepository.updatePassword(id, password);
    }

    @Override
    public void deleteUser(int id) {
        userRepository.deleteUser(id);
    }

    @Override
    public void enrollToCourse(int userId, int courseId) {
        boolean courseInUserCourses = userEnrolledToCourse(userId, courseId);

        if (!courseInUserCourses) {
            userRepository.enrollToCourse(userId, courseId, "enrolled", -1);
        }
    }

    @Override
    public boolean userEnrolledToCourse(int userId, int courseId) {
        User user = getUserById(userId);
        List<UserCourse> userCourses = user.getUserCourses();
        return userCourses.stream().anyMatch(x -> x.getCourse().getId() == courseId);
    }

    @Override
    public void completeCourse(int userId, int courseId, String status) {
        userRepository.completeCourse(userId, courseId, status);
    }

    @Override
    public void addRating(int courseId, Principal principal, int rating) {
        String name = principal.getName();
        User user = getUserByName(name);
        int userId = user.getId();
        Course course = courseService.getCourseById(courseId);
        UserCourse userCourse = userCourseService.getUserCourseByUserAndCourseId(userId, courseId);
        if (userCourse != null) {
            if (userCourse.getStatus().equals("completed") && userCourse.getAverageGrade() >= MIN_AVG_GRADE) {
                Rating ratingObj = new Rating();
                ratingObj.setRating(rating);
                ratingObj.setAuthor(user);
                ratingObj.setCourse(course);
                ratingService.create(ratingObj);
                double courseAverage = courseService.getAverageRating(courseId);
                course.setRating(courseAverage);
                courseService.update(courseId, course);
            } else {
                throw new IllegalArgumentException("INVALID STATUS OR LOW GRADE");
            }
        } else {
            throw new IllegalArgumentException("INVALID DATA");
        }
    }

    @Override
    public Page<User> findPaginatedUsers(Pageable pageable) {
        int pageSize = pageable.getPageSize();
        int currentPage = pageable.getPageNumber();
        int startItem = currentPage * pageSize;
        List<User> list;
        List<User> users = listUsers();
        if (users.size() < startItem) {
            list = Collections.emptyList();
        } else {
            int toIndex = Math.min(startItem + pageSize, users.size());
            list = users.subList(startItem, toIndex);
        }

        return new PageImpl<>(list, PageRequest.of(currentPage, pageSize), users.size());
    }
}