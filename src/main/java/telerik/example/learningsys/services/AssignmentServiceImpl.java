package telerik.example.learningsys.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import telerik.example.learningsys.models.Assignment;
import telerik.example.learningsys.repositories.interfaces.AssignmentRepository;
import telerik.example.learningsys.services.Interfaces.AssignmentService;

import java.util.List;

@Service
public class AssignmentServiceImpl implements AssignmentService {
    private AssignmentRepository assignmentRepository;

    @Autowired
    public AssignmentServiceImpl(AssignmentRepository assignmentRepository) {
        this.assignmentRepository = assignmentRepository;
    }

    @Override
    public List<Assignment> getAll() {
        return assignmentRepository.getAll();
    }

    @Override
    public Assignment getAssignmentById(int id) {
        return assignmentRepository.getAssignmentById(id);
    }

    @Override
    public void create(Assignment assignment) {
        assignmentRepository.create(assignment);
    }

    @Override
    public void update(int assignmentId, Assignment assignment) {
        assignment.setId(assignmentId);
        assignmentRepository.update(assignment);
    }

    @Override
    public void delete(int assignmentId) {
        Assignment assignment = assignmentRepository.getAssignmentById(assignmentId);
        assignmentRepository.delete(assignment);
    }
}
