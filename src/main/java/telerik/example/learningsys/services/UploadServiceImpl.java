package telerik.example.learningsys.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import telerik.example.learningsys.models.*;
import telerik.example.learningsys.repositories.interfaces.UserRepository;
import telerik.example.learningsys.services.Interfaces.*;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.Principal;

@Service
public class UploadServiceImpl implements UploadService {
    private static String STATIC_RESOURCES_DIR = System.getProperty("user.dir") + "/src/main/resources/static";
    private static String COURSES_UPLOAD_DIR = "/upload/";
    private static String ABSOLUTE_COURSES_UPLOAD_DIR = STATIC_RESOURCES_DIR + COURSES_UPLOAD_DIR;

    private UserRepository userRepository;
    private AssignmentService assignmentService;
    private SolutionService solutionService;
    private LectureService lectureService;
    private VideoService videoService;
    private UserCourseService userCourseService;

    @Autowired
    public UploadServiceImpl(UserRepository userRepository, AssignmentService assignmentService,
                             SolutionService solutionService, LectureService lectureService,
                             VideoService videoService, UserCourseService userCourseService) {
        this.userRepository = userRepository;
        this.assignmentService = assignmentService;
        this.solutionService = solutionService;
        this.lectureService = lectureService;
        this.videoService = videoService;
        this.userCourseService = userCourseService;
    }

    @Override
    public String writeFile(Principal principal, MultipartFile file, String parentDir, String uploadDir) throws java.io.IOException {
        if (file.isEmpty()) {
            throw new IllegalArgumentException();
        }
        String name = principal.getName();
        User user = userRepository.getUserByName(name);
        String directoryName = parentDir + "usr_" + user.getUsername() + "/" + file.getOriginalFilename();
        String relDirectoryName = uploadDir + "usr_" + user.getUsername() + "/" + file.getOriginalFilename();
        File tempFile = new File(directoryName);
        boolean fileExists = tempFile.exists();
        if (fileExists) {
            return relDirectoryName;
        }

        Path path = Paths.get(directoryName);
        Files.createDirectories(path.getParent());
        if(!Files.exists(path))
            Files.createFile(path);
        Files.write(path, file.getBytes());

        return relDirectoryName;
    }

    @Override
    public void uploadSolution(Principal principal, MultipartFile file, int assignmentId) {
        try {
            String name = principal.getName();
            User user = userRepository.getUserByName(name);
            Assignment assignment = assignmentService.getAssignmentById(assignmentId);
            Solution solution = new Solution();
            solution.setAssignment(assignment);
            solution.setUser(user);
            solution.setGrade(-1);
            int courseId = assignment.getLecture().getCourse().getId();
            solution.setUserCourse(userCourseService.getUserCourseByUserAndCourseId(user.getId(), courseId));
            solution.setFilePath(writeFile(principal, file, ABSOLUTE_COURSES_UPLOAD_DIR, COURSES_UPLOAD_DIR));
            solutionService.create(solution);
        } catch (Exception e) {
            throw new IllegalArgumentException(e.getMessage());
        }
    }

    @Override
    public Video uploadVideo(Principal principal, MultipartFile file, int lectureId) {
        Lecture lecture = lectureService.getLectureById(lectureId);
        Video video = new Video();
        try {
            String writePath = writeFile(principal, file, ABSOLUTE_COURSES_UPLOAD_DIR, COURSES_UPLOAD_DIR);
            video.setFilePath(writePath);
            video.setLecture(lecture);
            videoService.create(video);
            return video;
        } catch (Exception e) {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public Assignment uploadAssignment(Principal principal, MultipartFile file, int lectureId, String title) {
        Lecture lecture = lectureService.getLectureById(lectureId);
        Assignment assignment = new Assignment();
        String name = principal.getName();
        User user = userRepository.getUserByName(name);
        try {
            String writePath = writeFile(principal, file, ABSOLUTE_COURSES_UPLOAD_DIR, COURSES_UPLOAD_DIR);
            assignment.setFilePath(writePath);
            assignment.setLecture(lecture);
            assignment.setTitle(title);
            assignment.setAuthor(user);
            assignmentService.create(assignment);
            return assignment;
        } catch (Exception e) {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public Lecture addImageToLecture(Principal principal, MultipartFile file, Lecture lecture) {
        try {
            String writePath = writeFile(principal, file, ABSOLUTE_COURSES_UPLOAD_DIR, COURSES_UPLOAD_DIR);
            lecture.setImagePath(writePath);
            return lecture;
        } catch (Exception e) {
            throw new IllegalArgumentException();
        }
    }

}
