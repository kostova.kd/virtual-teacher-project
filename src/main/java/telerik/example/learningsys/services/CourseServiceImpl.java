package telerik.example.learningsys.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import telerik.example.learningsys.models.*;
import telerik.example.learningsys.repositories.interfaces.CourseRepository;
import telerik.example.learningsys.repositories.interfaces.RatingRepository;
import telerik.example.learningsys.services.Interfaces.CourseService;

import java.util.List;

@Service
public class CourseServiceImpl implements CourseService {
    private CourseRepository courseRepository;
    private RatingRepository ratingRepository;

    @Autowired
    public CourseServiceImpl(CourseRepository courseRepository, RatingRepository ratingRepository) {
        this.courseRepository = courseRepository;
        this.ratingRepository=ratingRepository;
    }

    @Override
    public List<Course> getAll() {
        return courseRepository.getAll();
    }

    @Override
    public Course getCourseById(int id) {
        Course course = courseRepository.getCourseById(id);
        if (course == null) {
            throw new IllegalArgumentException(String.format("Course with id %d not found.", id));
        }
        return course;
    }

    @Override
    public void create(Course course) {
        courseRepository.create(course);
    }

    @Override
    public void update(int courseId, Course course) {
        course.setId(courseId);
        courseRepository.update(course);
    }

    @Override
    public void delete(int courseId) {
        Course course = courseRepository.getCourseById(courseId);
        courseRepository.delete(course);
    }

    @Override
    public List<Lecture> getCourseLectures(int courseId) {
        return courseRepository.getCourseLectures(courseId);
    }

    @Override
    public double getAverageRating(int courseId) {
        return ratingRepository.getRatingsOfCourse(courseId).stream()
            .map(Rating::getRating)
            .mapToDouble(Integer::doubleValue)
            .average().orElse(0);
    }
}
