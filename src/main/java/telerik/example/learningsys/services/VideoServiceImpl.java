package telerik.example.learningsys.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import telerik.example.learningsys.models.Topic;
import telerik.example.learningsys.models.Video;
import telerik.example.learningsys.repositories.interfaces.TopicRepository;
import telerik.example.learningsys.repositories.interfaces.VideoRepository;
import telerik.example.learningsys.services.Interfaces.TopicService;
import telerik.example.learningsys.services.Interfaces.VideoService;

import java.util.List;

@Service
public class VideoServiceImpl implements VideoService {
    private VideoRepository videoRepository;

    @Autowired
    public VideoServiceImpl(VideoRepository videoRepository) {
        this.videoRepository = videoRepository;
    }

    @Override
    public List<Video> getAll() {
        return videoRepository.getAll();
    }

    @Override
    public void create(Video video) {
        videoRepository.create(video);
    }

    @Override
    public void update(int videoId, Video video) {
        video.setId(videoId);
        videoRepository.update(video);
    }

    @Override
    public void delete(int topicId) {
        Video video = videoRepository.getVideoById(topicId);
        videoRepository.delete(video);
    }
}