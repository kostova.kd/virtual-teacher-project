package telerik.example.learningsys.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import telerik.example.learningsys.models.Assignment;
import telerik.example.learningsys.models.Course;
import telerik.example.learningsys.models.Lecture;
import telerik.example.learningsys.models.Video;
import telerik.example.learningsys.repositories.interfaces.CourseRepository;
import telerik.example.learningsys.repositories.interfaces.LectureRepository;
import telerik.example.learningsys.services.Interfaces.LectureService;

import java.util.List;

@Service
public class LectureServiceImpl implements LectureService {
    private LectureRepository lectureRepository;
    private CourseRepository courseRepository;

    @Autowired
    public LectureServiceImpl(LectureRepository lectureRepository, CourseRepository courseRepository) {
        this.lectureRepository = lectureRepository;
        this.courseRepository = courseRepository;
    }

    @Override
    public List<Lecture> getAll() {
        return lectureRepository.getAll();
    }

    @Override
    public Lecture getLectureById(int id) {
        return lectureRepository.getLectureById(id);
    }

    @Override
    public void create(Lecture lecture) {
        lectureRepository.create(lecture);
    }

    @Override
    public void update(int lectureId, Lecture lecture) {
        lecture.setId(lectureId);
        lectureRepository.update(lecture);
    }

    @Override
    public void delete(int lectureId) {
        Lecture lecture = lectureRepository.getLectureById(lectureId);
        lectureRepository.delete(lecture);
    }

    @Override
    public void addCourseLecture(int id, Lecture lecture, Video video, Assignment assignment) {
        try {
            lecture.setVideo(video);
            lecture.setAssignment(assignment);
            Course course = courseRepository.getCourseById(id);
            lecture.setCourse(course);
            lectureRepository.create(lecture);
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException("Can't create a lecture");
        }
    }
}
