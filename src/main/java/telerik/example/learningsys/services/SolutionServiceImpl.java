package telerik.example.learningsys.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import telerik.example.learningsys.models.Solution;
import telerik.example.learningsys.repositories.interfaces.SolutionRepository;
import telerik.example.learningsys.services.Interfaces.SolutionService;

import java.util.List;

@Service
public class SolutionServiceImpl implements SolutionService {

    private SolutionRepository solutionRepository;

    @Autowired
    public SolutionServiceImpl(SolutionRepository solutionRepository) {
        this.solutionRepository = solutionRepository;
    }

    @Override
    public List<Solution> getAll() {
        return solutionRepository.getAll();
    }

    @Override
    public List<Solution> getSolutionsByAssignmentAndUserId(int authorid) {
        return solutionRepository.getSolutionsByAssignmentAndUserId(authorid);
    }

    @Override
    public Solution getById(int id) {
        return solutionRepository.getSolutionById(id);
    }

    @Override
    public void create(Solution solution) {
        if (solution == null) {
            throw new IllegalArgumentException("Missing data in one or more fields!");
        }
        if (solution.getAssignment()==null || solution.getUser()==null || solution.getFilePath().isEmpty()) {
            throw new IllegalArgumentException("Missing data in one or more fields!");
        }
        solutionRepository.create(solution);
    }

    @Override
    public void update(int solutionId, Solution solution) {
        solution.setId(solutionId);
        solutionRepository.update(solution);
    }

    @Override
    public void delete(int solutionId) {
        Solution solution = solutionRepository.getSolutionById(solutionId);
        solutionRepository.delete(solution);
    }

    @Override
    public List<Solution> getSolutionsByUserCourseId(int usercourseid) {
        return solutionRepository.getSolutionsByUserCourseId(usercourseid);
    }
}
