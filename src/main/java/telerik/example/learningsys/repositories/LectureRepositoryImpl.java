package telerik.example.learningsys.repositories;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import telerik.example.learningsys.models.Assignment;
import telerik.example.learningsys.models.Course;
import telerik.example.learningsys.models.Lecture;
import telerik.example.learningsys.repositories.interfaces.LectureRepository;

import java.util.List;

@Repository
@Transactional
public class LectureRepositoryImpl implements LectureRepository {
    private SessionFactory sessionFactory;

    @Autowired
    public LectureRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Lecture getLectureById(int id) {
        Session session = setupSession();
        return session.get(Lecture.class, id);
    }

    @Override
    public List<Lecture> getAll() {
        Session session = setupSession();
        Query<Lecture> query = session.createQuery("from Lecture", Lecture.class);
        return query.list();
    }

    @Override
    public void create(Lecture lecture) {
        Session session = setupSession();
        session.save(lecture);
    }

    @Override
    public void update(Lecture lecture) {
        Session session = setupSession();
        session.update(lecture);
    }

    @Override
    public void delete(Lecture lecture) {
        Session session = setupSession();
        session.delete(lecture);
    }

    private Session setupSession() {
        Session session;
        try {
            session = sessionFactory.getCurrentSession();
        } catch (Exception e) {
            session = sessionFactory.openSession();
        }
        return session;
    }
}
