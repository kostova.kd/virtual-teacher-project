package telerik.example.learningsys.repositories;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import telerik.example.learningsys.models.Solution;
import telerik.example.learningsys.repositories.interfaces.SolutionRepository;

import java.util.List;

@Repository
@Transactional
public class SolutionRepositoryImpl implements SolutionRepository {

    private SessionFactory sessionFactory;

    @Autowired
    public SolutionRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Solution getSolutionById(int id) {
        Session session = setupSession();
        return session.get(Solution.class, id);
    }

    @Override
    public List<Solution> getSolutionsByAssignmentAndUserId(int authorId) {
        Session session = setupSession();
        Query<Solution> query = session.createNativeQuery("select s.* from solutions s inner join assignments a " +
                "on s.assignmentid=a.assignmentid " +
                "where authorid=:authorid", Solution.class);
        query.setParameter("authorid", authorId);
        return query.list();
    }

    @Override
    public List<Solution> getAll() {
        Session session = setupSession();
        Query<Solution> query = session.createQuery("from Solution", Solution.class);
        return query.list();
    }

    @Override
    public List<Solution> getSolutionsByUserCourseId(int userCourseId) {
        Session session = setupSession();
        Query<Solution> query = session.createQuery("from Solution where usercourseid=:usercourseid", Solution.class);
        query.setParameter("usercourseid", userCourseId);
        return query.list();
    }

    @Override
    public void create(Solution solution) {
        Session session = setupSession();
        session.save(solution);
    }

    @Override
    public void update(Solution solution) {
        Session session = setupSession();
        session.update(solution);
    }

    @Override
    public void delete(Solution solution) {
        Session session = setupSession();
        session.delete(solution);
    }

    private Session setupSession() {
        Session session;
        try {
            session = sessionFactory.getCurrentSession();
        } catch (Exception e) {
            session = sessionFactory.openSession();
        }
        return session;
    }
}
