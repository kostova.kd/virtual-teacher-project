package telerik.example.learningsys.repositories;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import telerik.example.learningsys.models.Rating;
import telerik.example.learningsys.repositories.interfaces.RatingRepository;

import java.util.List;

@Repository
@Transactional
public class RatingRepositoryImpl implements RatingRepository {
    private SessionFactory sessionFactory;

    @Autowired
    public RatingRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Rating getRatingById(int id) {
        Session session = setupSession();
        return session.get(Rating.class, id);
    }

    @Override
    public List<Rating> getAll() {
        Session session = setupSession();
        Query<Rating> query = session.createQuery("from Rating", Rating.class);
        return query.list();
    }

    @Override
    public List<Rating> getRatingsOfCourse(int courseid) {
        Session session = setupSession();
        Query<Rating> query = session.createQuery("from Rating where courseid=:courseid", Rating.class);
        query.setParameter("courseid", courseid);
        return query.list();
    }

    @Override
    public void create(Rating rating) {
        Session session = setupSession();
        session.save(rating);
    }

    @Override
    public void update(Rating rating) {
        Session session = setupSession();
        session.update(rating);
    }

    @Override
    public void delete(Rating rating) {
        Session session = setupSession();
        session.delete(rating);
    }

    private Session setupSession() {
        Session session;
        try {
            session = sessionFactory.getCurrentSession();
        } catch (Exception e) {
            session = sessionFactory.openSession();
        }
        return session;
    }
}
