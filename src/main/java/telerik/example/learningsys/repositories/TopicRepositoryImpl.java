package telerik.example.learningsys.repositories;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import telerik.example.learningsys.models.Topic;
import telerik.example.learningsys.repositories.interfaces.TopicRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public class TopicRepositoryImpl implements TopicRepository {

    private SessionFactory sessionFactory;

    @Autowired
    public TopicRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Topic getTopicById(int id) {
        Session session = setupSession();
        return session.get(Topic.class, id);
    }

    @Override
    public List<Topic> getAll() {
        Session session = setupSession();
        Query<Topic> query = session.createQuery("from Topic", Topic.class);
        return query.list();
    }

    @Override
    public void create(Topic topic) {
        Session session = setupSession();
        session.save(topic);
    }

    @Override
    public void update(Topic topic) {
        Session session = setupSession();
        session.update(topic);
    }

    @Override
    public void delete(Topic topic) {
        Session session = setupSession();
        session.delete(topic);
    }

    private Session setupSession() {
        Session session;
        try {
            session = sessionFactory.getCurrentSession();
        } catch (Exception e) {
            session = sessionFactory.openSession();
        }
        return session;
    }
}
