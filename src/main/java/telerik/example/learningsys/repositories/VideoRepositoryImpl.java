package telerik.example.learningsys.repositories;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import telerik.example.learningsys.models.Video;
import telerik.example.learningsys.repositories.interfaces.VideoRepository;

import java.util.List;

@Repository
@Transactional
public class VideoRepositoryImpl implements VideoRepository {

    private SessionFactory sessionFactory;

    @Autowired
    public VideoRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Video getVideoById(int id) {
        Session session = setupSession();
        return session.get(Video.class, id);
    }

    @Override
    public List<Video> getAll() {
        Session session = setupSession();
        Query<Video> query = session.createQuery("from Video", Video.class);
        return query.list();
    }

    @Override
    public void create(Video video) {
        Session session = setupSession();
        session.save(video);
    }

    @Override
    public void update(Video video) {
        Session session = setupSession();
        session.update(video);
    }

    @Override
    public void delete(Video video) {
        Session session = setupSession();
        session.delete(video);
    }

    private Session setupSession() {
        Session session;
        try {
            session = sessionFactory.getCurrentSession();
        } catch (Exception e) {
            session = sessionFactory.openSession();
        }
        return session;
    }

}
