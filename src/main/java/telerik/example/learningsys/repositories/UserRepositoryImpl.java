package telerik.example.learningsys.repositories;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import telerik.example.learningsys.models.Course;
import telerik.example.learningsys.models.User;
import telerik.example.learningsys.repositories.interfaces.UserRepository;

import java.security.Principal;
import java.util.List;

@Repository
@Transactional
public class UserRepositoryImpl implements UserRepository {
    private SessionFactory sessionFactory;

    @Autowired
    public UserRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<User> listUsers() {
        Session session = sessionFactory.openSession();
        Query<User> query = session.createQuery("from User", User.class);
        return query.list();
    }

    @Override
    public User getUserById(int id) {
        Session session = sessionFactory.openSession();
        return session.get(User.class, id);
    }

    @Override
    public User getUserByName(String name) {
        Session session = sessionFactory.getCurrentSession();
        Query<User> query = session.createQuery("from User where username = :name", User.class);
        query.setParameter("name", name);
        return query.list().stream().findFirst().orElse(null);
    }

    @Override
    public String getUserAuthorityById(int id) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createSQLQuery("select authority from authorities where userid = :id");
        query.setParameter("id", id);
        return query.getSingleResult().toString();
    }

    @Override
    public void createUser(User user, String authority) {
        Session session = sessionFactory.getCurrentSession();
        session.save(user);
        Query query = session.createSQLQuery("insert into authorities (userid, authority) values (:userid, :authority)");
        query.setParameter("userid", user.getId());
        query.setParameter("authority", authority);
        query.executeUpdate();
    }

    @Override
    public void updateUser(int id, User user, String authority) {
        Session session = sessionFactory.getCurrentSession();

        Query query = session.createSQLQuery("delete from authorities where userid = :id");
        query.setParameter("id", id);
        query.executeUpdate();

        query = session.createSQLQuery("insert into authorities (userid, authority) values (:userid, :authority)");
        query.setParameter("userid", user.getId());
        query.setParameter("authority", authority);
        query.executeUpdate();

        user.setUsername(getUserById(id).getUsername());
        session.update(user);
    }

    @Override
    public void updatePassword(int id, String password)  {
        Session session = sessionFactory.getCurrentSession();
        User user = session.get(User.class, id);
        user.setPassword(password);
        session.update(user);
    }

    @Override
    public void deleteUser(int id) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createSQLQuery("delete from authorities where userid = :id");
        query.setParameter("id", id);
        query.executeUpdate();

        query = session.createSQLQuery("delete from solutions where userid = :id");
        query.setParameter("id", id);
        query.executeUpdate();

        query = session.createSQLQuery("delete from user_courses where userid = :id");
        query.setParameter("id", id);
        query.executeUpdate();

        User user = session.get(User.class, id);
        session.delete(user);
    }

    @Override
    public void enrollToCourse(int userId, int courseId, String status, double averageGrade) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createSQLQuery("insert into user_courses (userid, courseid, status, average_grade)" +
                "values (:userid, :courseid, :status, :averageGrade)");
        query.setParameter("userid", userId);
        query.setParameter("courseid", courseId);
        query.setParameter("status", status);
        query.setParameter("averageGrade", averageGrade);
        query.executeUpdate();
    }

    @Override
    public void completeCourse(int userId, int courseId, String status) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createSQLQuery("insert into user_courses (status)" +
                "values (:status) where userid = :userid and courseid = :courseid");
        query.setParameter("status", status);
        query.setParameter("userid", userId);
        query.setParameter("courseid", courseId);
        query.executeUpdate();
    }
}