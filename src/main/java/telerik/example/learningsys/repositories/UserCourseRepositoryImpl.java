package telerik.example.learningsys.repositories;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import telerik.example.learningsys.models.UserCourse;
import telerik.example.learningsys.repositories.interfaces.UserCourseRepository;

import java.util.List;

@Repository
@Transactional
public class UserCourseRepositoryImpl implements UserCourseRepository {
    private SessionFactory sessionFactory;

    @Autowired
    public UserCourseRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public UserCourse getUserCourseById(int userCourseId) {
        Session session = setupSession();
        return session.get(UserCourse.class, userCourseId);
    }

    @Override
    public List<UserCourse> getAll() {
        Session session = setupSession();
        Query<UserCourse> query = session.createQuery("from UserCourse", UserCourse.class);
        return query.list();
    }

    @Override
    public void updateUserCourse(UserCourse userCourse) {
        Session session = setupSession();
        session.update(userCourse);
    }

    @Override
    public UserCourse getUserCourseByUserAndCourseId(int userId, int courseId) {
        Session session = setupSession();
        Query<UserCourse> query = session.createNativeQuery("select * from user_courses where userid = :userid and courseid = :courseid", UserCourse.class);
        query.setParameter("userid", userId);
        query.setParameter("courseid", courseId);
        return query.list().stream().findFirst().orElse(null);
    }

    @Override
    public List<UserCourse> getUserCoursesByStatus(int userId, String status) {
        Session session = setupSession();
        Query<UserCourse> query = session.createNativeQuery("select * from user_courses where userid = :userid and status = :status", UserCourse.class);
        query.setParameter("userid", userId);
        query.setParameter("status", status);
        return query.list();
    }

    private Session setupSession() {
        Session session;
        try {
            session = sessionFactory.getCurrentSession();
        } catch (Exception e) {
            session = sessionFactory.openSession();
        }
        return session;
    }
}
