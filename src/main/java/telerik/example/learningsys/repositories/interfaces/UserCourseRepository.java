package telerik.example.learningsys.repositories.interfaces;

import telerik.example.learningsys.models.UserCourse;

import java.util.List;

public interface UserCourseRepository {
    UserCourse getUserCourseById(int userCourseId);
    List<UserCourse> getAll();
    void updateUserCourse(UserCourse userCourse);
    UserCourse getUserCourseByUserAndCourseId(int userId, int courseId);
    List<UserCourse> getUserCoursesByStatus(int userId, String status);
}
