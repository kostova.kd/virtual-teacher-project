package telerik.example.learningsys.repositories.interfaces;

import telerik.example.learningsys.models.Solution;

import java.util.List;

public interface SolutionRepository {

    Solution getSolutionById(int id);
    List<Solution> getAll();
    List<Solution> getSolutionsByUserCourseId(int userCourseId);
    List<Solution> getSolutionsByAssignmentAndUserId(int authorId);
    void create(Solution solution);
    void update(Solution solution);
    void delete(Solution solution);

}
