package telerik.example.learningsys.repositories.interfaces;

import telerik.example.learningsys.models.Video;

import java.util.List;

public interface VideoRepository {

    Video getVideoById(int id);
    List<Video> getAll();
    void create(Video video);
    void update(Video video);
    void delete(Video video);
}
