package telerik.example.learningsys.repositories.interfaces;

import telerik.example.learningsys.models.Assignment;

import java.util.List;

public interface AssignmentRepository {

    Assignment getAssignmentById(int id);
    List<Assignment> getAll();
    void create(Assignment assignment);
    void update(Assignment assignment);
    void delete(Assignment assignment);
}
