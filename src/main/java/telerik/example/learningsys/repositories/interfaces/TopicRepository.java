package telerik.example.learningsys.repositories.interfaces;

import telerik.example.learningsys.models.Topic;

import java.util.List;

public interface TopicRepository {

    Topic getTopicById(int id);
    List<Topic> getAll();
    void create(Topic topic);
    void update(Topic topic);
    void delete(Topic topic);
}
