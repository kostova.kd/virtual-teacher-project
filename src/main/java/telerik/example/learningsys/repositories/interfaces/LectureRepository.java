package telerik.example.learningsys.repositories.interfaces;

import telerik.example.learningsys.models.Assignment;
import telerik.example.learningsys.models.Lecture;

import java.util.List;

public interface LectureRepository {

    Lecture getLectureById(int id);
    List<Lecture> getAll();
    void create(Lecture lecture);
    void update(Lecture lecture);
    void delete(Lecture lecture);
}
