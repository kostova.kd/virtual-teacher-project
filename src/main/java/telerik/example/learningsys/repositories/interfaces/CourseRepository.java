package telerik.example.learningsys.repositories.interfaces;

import telerik.example.learningsys.models.Course;
import telerik.example.learningsys.models.Lecture;

import java.util.List;

public interface CourseRepository {

    Course getCourseById(int id);
    List<Course> getAll();
    List<Lecture> getCourseLectures(int courseId);
    void create(Course course);
    void update(Course course);
    void delete(Course course);
}
