package telerik.example.learningsys.repositories.interfaces;

import telerik.example.learningsys.models.User;

import java.util.List;

public interface UserRepository {

    List<User> listUsers();
    User getUserById(int id);
    User getUserByName(String name);
    String getUserAuthorityById(int id);
    void createUser(User user, String authority);
    void updateUser(int id, User user, String authority);
    void updatePassword(int id, String password);
    void deleteUser(int id);
    void enrollToCourse(int userId, int courseId, String status, double averageGrade);
    void completeCourse(int userId, int courseId, String status);
}
