package telerik.example.learningsys.repositories.interfaces;

import telerik.example.learningsys.models.Rating;

import java.util.List;

public interface RatingRepository {

    Rating getRatingById(int id);
    List<Rating> getAll();
    List<Rating> getRatingsOfCourse(int courseid);
    void create(Rating rating);
    void update(Rating rating);
    void delete(Rating rating);
}
