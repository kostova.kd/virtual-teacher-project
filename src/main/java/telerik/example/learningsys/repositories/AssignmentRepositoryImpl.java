package telerik.example.learningsys.repositories;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import telerik.example.learningsys.models.Assignment;
import telerik.example.learningsys.models.Lecture;
import telerik.example.learningsys.repositories.interfaces.AssignmentRepository;

import java.util.List;

@Repository
@Transactional
public class AssignmentRepositoryImpl implements AssignmentRepository {
    private SessionFactory sessionFactory;

    @Autowired
    public AssignmentRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Assignment getAssignmentById(int id) {
        Session session = setupSession();
        return session.get(Assignment.class, id);
    }

    @Override
    public List<Assignment> getAll() {
        Session session = setupSession();
        Query<Assignment> query = session.createQuery("from Assignment", Assignment.class);
        return query.list();
    }

    @Override
    public void create(Assignment assignment) {
        Session session = setupSession();
        session.save(assignment);
    }

    @Override
    public void update(Assignment assignment) {
        Session session = setupSession();
        session.update(assignment);
    }

    @Override
    public void delete(Assignment assignment) {
        Session session = setupSession();
        session.delete(assignment);
    }

    private Session setupSession() {
        Session session;
        try {
            session = sessionFactory.getCurrentSession();
        } catch (Exception e) {
            session = sessionFactory.openSession();
        }
        return session;
    }
}
