package telerik.example.learningsys.repositories;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import telerik.example.learningsys.models.Course;
import telerik.example.learningsys.models.Lecture;
import telerik.example.learningsys.models.Video;
import telerik.example.learningsys.repositories.interfaces.CourseRepository;

import java.util.List;

@Repository
@Transactional
public class CourseRepositoryImpl implements CourseRepository {
    private SessionFactory sessionFactory;

    @Autowired
    public CourseRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Course getCourseById(int id) {
        Session session = setupSession();
        return session.get(Course.class, id);
    }

    @Override
    public List<Course> getAll() {
        Session session = setupSession();
        Query<Course> query = session.createQuery("from Course", Course.class);
        return query.list();
    }

    @Override
    public void create(Course course) {
        Session session = setupSession();
        session.save(course);
    }

    @Override
    public void update(Course course) {
        Session session = setupSession();
        session.update(course);
    }

    @Override
    public void delete(Course course) {
        Session session = setupSession();
        session.delete(course);
    }

    @Override
    public List<Lecture> getCourseLectures(int courseId) {
        Session session = setupSession();
        Query<Lecture> query = session.createNativeQuery("select l.* from lectures l join courses c " +
                "on l.courseid = c.courseid where l.courseid = :courseid", Lecture.class);
        query.setParameter("courseid", courseId);
        return query.list();
    }

    private Session setupSession() {
        Session session;
        try {
            session = sessionFactory.getCurrentSession();
        } catch (Exception e) {
            session = sessionFactory.openSession();
        }
        return session;
    }
}
