package telerik.example.learningsys.controllersREST;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import telerik.example.learningsys.models.Course;
import telerik.example.learningsys.models.Lecture;
import telerik.example.learningsys.services.Interfaces.CourseService;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/courses")
public class CourseController {
    private CourseService service;

    @Autowired
    public CourseController(CourseService service) {
        this.service = service;
    }

    @GetMapping
    public List<Course> getAll() {
        return service.getAll();
    }

    @GetMapping("/{courseId}/lectures")
    public List<Lecture> getAllCourseLectures(@PathVariable int courseId) {
        return service.getCourseLectures(courseId);
    }

    @PostMapping("/new")
    public void create(@Valid @RequestBody Course course) {
        try {
            service.create(course);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/{courseId}")
    public void update(@PathVariable int courseId, @Valid @RequestBody Course course) {
        try {
            service.update(courseId, course);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @DeleteMapping("/{courseId}")
    public void delete(@PathVariable int courseId) {
        service.delete(courseId);
    }
}
