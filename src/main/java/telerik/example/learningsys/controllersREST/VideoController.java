package telerik.example.learningsys.controllersREST;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import telerik.example.learningsys.models.Video;
import telerik.example.learningsys.services.Interfaces.VideoService;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/videos")
public class VideoController {

    private VideoService service;

    @Autowired
    public VideoController(VideoService service) {
        this.service = service;
    }

    @GetMapping
    public List<Video> getAll() {
        return service.getAll();
    }

    @PostMapping("/new")
    public void create(@Valid @RequestBody Video video) {
        try {
            service.create(video);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/{videoId}")
    public void update(@PathVariable int videoId, @Valid @RequestBody Video video) {
        try {
            service.update(videoId, video);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }

    }

    @DeleteMapping("/{videoId}")
    public void delete(@PathVariable int videoId) {
        service.delete(videoId);
    }
}
