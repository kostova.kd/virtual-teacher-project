package telerik.example.learningsys.controllersREST;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import telerik.example.learningsys.models.User;
import telerik.example.learningsys.services.UserServiceImpl;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("api/users")
public class UserController {
    private UserServiceImpl userService;

    @Autowired
    public UserController(UserServiceImpl userService) {
        this.userService = userService;
    }

    @GetMapping
    public List<User> getAll() {
        return userService.listUsers();
    }

    @GetMapping("/{id}")
    public User getUserById(@PathVariable int id) {
        try {
            return userService.getUserById(id);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/{name}")
    public User getUserByName(@PathVariable String name) {
        try {
            return userService.getUserByName(name);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping("/new")
    @ResponseStatus(HttpStatus.CREATED)
    public @ResponseBody
    User createUser(@Valid @RequestBody User user) {
        try {
            userService.createUser(user, "ROLE_USER");
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
        return user;
    }

    @DeleteMapping("/delete/{id}")
    public void deleteUser(@PathVariable int id) {
        userService.deleteUser(id);
    }

    @GetMapping("/{userid}/enrolled-courses/{courseid}")
    public String enrollToCourse(@PathVariable int userid, @PathVariable int courseid) {
        try {
            userService.enrollToCourse(userid, courseid);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
        return String.format("user with id %s added course with id %s", userid, courseid);
    }

    @PostMapping("/{userid}/completed-courses/{courseid}")
    @ResponseStatus(HttpStatus.CREATED)
    public void completeCourse(@PathVariable int userid, @PathVariable int courseid) {
        try {
            userService.completeCourse(userid, courseid, "COMPLETED");
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

}