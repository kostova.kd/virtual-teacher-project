package telerik.example.learningsys.controllersREST;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import telerik.example.learningsys.models.Lecture;
import telerik.example.learningsys.models.Topic;
import telerik.example.learningsys.services.Interfaces.LectureService;
import telerik.example.learningsys.services.Interfaces.TopicService;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/topics")
public class TopicController {

        private TopicService service;

        @Autowired
        public TopicController(TopicService service) {
            this.service = service;
        }

        @GetMapping
        public List<Topic> getAll() {
            return service.getAll();
        }

        @PostMapping("/new")
        public void create(@Valid @RequestBody Topic topic) {
            try {
                service.create(topic);
            } catch (IllegalArgumentException e) {
                throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
            }
        }

        @PutMapping("/{topicId}")
        public void update(@PathVariable int topicId, @Valid @RequestBody Topic topic) {
            try {
                service.update(topicId, topic);
            } catch (IllegalArgumentException e) {
                throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
            }

        }

        @DeleteMapping("/{topicId}")
        public void delete(@PathVariable int topicId) {
            service.delete(topicId);
        }


}
