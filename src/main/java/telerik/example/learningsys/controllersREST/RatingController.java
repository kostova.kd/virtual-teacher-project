package telerik.example.learningsys.controllersREST;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import telerik.example.learningsys.models.Lecture;
import telerik.example.learningsys.models.Rating;
import telerik.example.learningsys.services.Interfaces.RatingService;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("api/ratings")
public class RatingController {
    private RatingService service;

    @Autowired
    public RatingController(RatingService service) {
        this.service = service;
    }

    @GetMapping
    public List<Rating> getAll() {
        return service.getAll();
    }

    @PostMapping("/new")
    public void create(@Valid @RequestBody Rating rating) {
        try {
            service.create(rating);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/{ratingId}")
    public void update(@PathVariable int ratingId, @Valid @RequestBody Rating rating) {
        try {
            service.update(ratingId, rating);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }

    }

    @DeleteMapping("/{ratingId}")
    public void delete(@PathVariable int ratingId) {
        service.delete(ratingId);
    }
}
