package telerik.example.learningsys.controllersREST;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import telerik.example.learningsys.models.Lecture;
import telerik.example.learningsys.services.Interfaces.LectureService;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/lectures")
public class LectureController {
    private LectureService service;

    @Autowired
    public LectureController(LectureService service) {
        this.service = service;
    }

    @GetMapping
    public List<Lecture> getAll() {
        return service.getAll();
    }

    @PostMapping("/new")
    public void create(@Valid @RequestBody Lecture lecture) {
        try {
            service.create(lecture);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/{lectureId}")
    public void update(@PathVariable int lectureId, @Valid @RequestBody Lecture lecture) {
        try {
            service.update(lectureId, lecture);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }

    }

    @DeleteMapping("/{lectureId}")
    public void delete(@PathVariable int lectureId) {
        service.delete(lectureId);
    }
}
