package telerik.example.learningsys.controllersREST;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import telerik.example.learningsys.models.Solution;
import telerik.example.learningsys.services.Interfaces.SolutionService;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/solutions")
public class SolutionController {

    private SolutionService service;

    @Autowired
    public SolutionController(SolutionService service) {
        this.service = service;
    }

    @GetMapping
    public List<Solution> getAll() {
        return service.getAll();
    }

    @PostMapping("/new")
    public void create(@Valid @RequestBody Solution solution) {
        try {
            service.create(solution);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/{solutionId}")
    public void update(@PathVariable int solutionId, @Valid @RequestBody Solution solution) {
        try {
            service.update(solutionId, solution);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }

    }

    @DeleteMapping("/{solutionId}")
    public void delete(@PathVariable int solutionId) {
        service.delete(solutionId);
    }
}
