package telerik.example.learningsys.controllersREST;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import telerik.example.learningsys.models.Assignment;
import telerik.example.learningsys.services.Interfaces.AssignmentService;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/assignments")
public class AssignmentController {
    private AssignmentService service;

    @Autowired
    public AssignmentController(AssignmentService service) {
        this.service = service;
    }

    @GetMapping
    public List<Assignment> getAll() {
        return service.getAll();
    }

    @PostMapping("/new")
    public void create(@Valid @RequestBody Assignment assignment) {
        try {
            service.create(assignment);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/{assignmentId}")
    public void update(@PathVariable int assignmentId, @Valid @RequestBody Assignment assignment) {
        try {
            service.update(assignmentId, assignment);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }

    }

    @DeleteMapping("/{assignmentId}")
    public void delete(@PathVariable int assignmentId) {
        service.delete(assignmentId);
    }
}
