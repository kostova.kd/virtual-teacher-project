package telerik.example.learningsys.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "lectures")
public class Lecture {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "lectureid")
    private int id;

    @Column(name = "title")
    @NotNull(message = "field required")
    @Size(min = 3, max = 50, message = "title must be between 3 and 50 characters.")
    private String title;

    @NotNull
    @Column(name = "description")
    private String description;

    @Column(name = "imagepath")
    private String imagePath;

    @OneToOne
    @JoinColumn(name = "assignmentid")
    private Assignment assignment;

    @OneToOne
    @JoinColumn(name = "videoid")
    private Video video;

    @ManyToOne
    @NotNull(message = "field required")
    @JoinColumn(name = "courseid")
    private Course course;

    public Lecture() {

    }

    public Lecture(String title, String description, Assignment assignment, Video video, Course course) {
        this.title = title;
        this.description = description;
        this.assignment = assignment;
        this.video = video;
        this.course = course;
    }

    public Lecture(String title, String description, Assignment assignment, String imagePath, Video video, Course course) {
        this.title = title;
        this.description = description;
        this.assignment = assignment;
        this.imagePath = imagePath;
        this.video = video;
        this.course = course;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Assignment getAssignment() {
        return assignment;
    }

    public void setAssignment(Assignment assignment) {
        this.assignment = assignment;
    }

    public Video getVideo() {
        return video;
    }

    public void setVideo(Video video) {
        this.video = video;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }
}

