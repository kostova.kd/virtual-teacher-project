package telerik.example.learningsys.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
@Table(name = "topics")
public class Topic {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "topicid")
    private int id;

    @NotNull
    @Size(min = 3, max = 20, message = "Topic must be between 3 and 20 characters")
    @Column(name = "topic")
    private String topic;

    @OneToMany(mappedBy = "topic", fetch = FetchType.EAGER, cascade=CascadeType.ALL)
    @JsonIgnore
    private List<Course> courses;

    public Topic() {
    }

    public Topic(String topic) {
        this.topic = topic;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public List<Course> getCourses() {
        return courses;
    }

    public void setCourses(List<Course> courses) {
        this.courses = courses;
    }
}