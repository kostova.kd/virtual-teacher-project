package telerik.example.learningsys.models;

import org.hibernate.validator.constraints.Range;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "ratings")
public class Rating {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ratingid")
    private int id;

    @NotNull
    @Range(min = 1, max = 5)
    @Column(name = "rating")
    private int rating;

    @ManyToOne
    @JoinColumn(name = "userid")
    private User author;

    @ManyToOne
    @JoinColumn(name = "courseid")
    private Course course;

    public Rating() {
    }

    public Rating(int rating, User author, Course course) {
        this.rating = rating;
        this.author = author;
        this.course = course;
    }

    public Rating(int id, int rating, User author, Course course) {
        this.id = id;
        this.rating = rating;
        this.author = author;
        this.course = course;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }
}
