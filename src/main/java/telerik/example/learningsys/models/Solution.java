package telerik.example.learningsys.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "solutions")
public class Solution {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "solutionid")
    private int id;

    @Column(name = "filepath")
    @NotNull(message = "field required")
    private String filePath;

    @ManyToOne(fetch=FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "assignmentid")
    private Assignment assignment;

    @ManyToOne(fetch=FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "userid")
    private User author;

    @Column(name = "grade")
    private Integer grade;

    @ManyToOne
    @JoinColumn(name = "usercourseid")
    private UserCourse userCourse;

    public  Solution() {
    }

    public Solution(String filePath, Assignment assignment, User author) {
        this.filePath = filePath;
        this.assignment = assignment;
        this.author = author;
    }

    public Solution(String filePath, Assignment assignment, User author, int grade) {
        this.filePath = filePath;
        this.assignment = assignment;
        this.author = author;
        this.grade=grade;
    }

    public Solution(String filePath, Assignment assignment, User author, Integer grade, UserCourse userCourse) {
        this.filePath = filePath;
        this.assignment = assignment;
        this.author = author;
        this.grade = grade;
        this.userCourse = userCourse;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public Assignment getAssignment() {
        return assignment;
    }

    public void setAssignment(Assignment assignment) {
        this.assignment = assignment;
    }

    public User getUser() {
        return author;
    }

    public void setUser(User author) {
        this.author = author;
    }

    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public UserCourse getUserCourse() {
        return userCourse;
    }

    public void setUserCourse(UserCourse userCourse) {
        this.userCourse = userCourse;
    }
}
