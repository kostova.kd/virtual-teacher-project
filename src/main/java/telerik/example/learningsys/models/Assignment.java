package telerik.example.learningsys.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
@Table(name = "assignments")
public class Assignment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "assignmentid")
    private int id;

    @Column(name = "title")
    @NotNull(message = "field required")
    @Size(min = 3, max = 50, message = "title must be between 3 and 50 characters.")
    private String title;

    @Column(name = "filepath")
    @NotNull(message = "field required")
    private String filePath;

    @OneToMany(mappedBy = "assignment")
    @JsonIgnore
    private List<Solution> solutions;

    @OneToOne(mappedBy = "assignment")
    private Lecture lecture;

    @ManyToOne
    @JoinColumn(name = "authorid")
    private User author;

    public Assignment() {

    }

    public Assignment(String title, String filePath, Lecture lecture) {
        this.title = title;
        this.filePath = filePath;
        this.lecture = lecture;
    }

    public Assignment(String title, String filePath, Lecture lecture, User author) {
        this.title = title;
        this.filePath = filePath;
        this.lecture = lecture;
        this.author = author;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public List<Solution> getSolutions() {
        return solutions;
    }

    public void setSolutions(List<Solution> solutions) {
        this.solutions = solutions;
    }

    public Lecture getLecture() {
        return lecture;
    }

    public void setLecture(Lecture lecture) {
        this.lecture = lecture;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }
}
