package telerik.example.learningsys.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
@Table(name = "courses")
public class Course {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "courseid")
    private int id;

    @Column(name = "title")
    @NotNull(message = "field required")
    @Size(min = 2, max = 50, message = "title must be between 2 and 50 characters.")
    private String title;

    @NotNull
    @Column(name = "description")
    private String description;

    @Column(name = "program")
    private String program;

    @NotNull
    @Column(name = "imagepath")
    private String imagePath;

    @Column(name = "rating")
    private double rating;

    @ManyToOne
    @JoinColumn(name = "topicid")
    private Topic topic;

    @ManyToOne
    @JoinColumn(name = "userid")
    private User teacher;

    @OneToMany(mappedBy = "course", fetch = FetchType.LAZY)
    @JsonIgnore
    private List<UserCourse> userCourses;

    @OneToMany(mappedBy = "course", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @Fetch(value = FetchMode.SUBSELECT)
    @JsonIgnore
    private List<Lecture> lectures;

    @OneToMany(mappedBy = "course")
    @JsonIgnore
    private List<Rating> ratings;

    public Course() {

    }

    public Course(String title, Topic topic, String description, String program, User teacher) {
        this.title = title;
        this.topic = topic;
        this.description = description;
        this.program = program;
        this.teacher = teacher;
    }

    public Course(String title, Topic topic, String description, String program, String imagePath, User teacher) {
        this.title = title;
        this.topic = topic;
        this.description = description;
        this.program = program;
        this.imagePath = imagePath;
        this.teacher = teacher;
    }


    public Course(String title, Topic topic, String description, String program, String imagePath, User teacher, double rating) {
        this.title = title;
        this.topic = topic;
        this.description = description;
        this.program = program;
        this.imagePath = imagePath;
        this.teacher = teacher;
        this.rating = rating;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Topic getTopic() {
        return topic;
    }

    public void setTopic(Topic topic) {
        this.topic = topic;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Lecture> getLectures() {
        return lectures;
    }

    public void setLectures(List<Lecture> lectures) {
        this.lectures = lectures;
    }

    public List<Rating> getRatings() {
        return ratings;
    }

    public void setRatings(List<Rating> ratings) {
        this.ratings = ratings;
    }

    public String getProgram() {
        return program;
    }

    public void setProgram(String program) {
        this.program = program;
    }

    public List<UserCourse> getUserCourses() {
        return userCourses;
    }

    public void setUserCourses(List<UserCourse> userCourses) {
        this.userCourses = userCourses;
    }

    public User getTeacher() {
        return teacher;
    }

    public void setTeacher(User teacher) {
        this.teacher = teacher;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }
}
