package telerik.example.learningsys.models;


import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
@Table(name = "videos")
public class Video {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "videoid")
    private int id;

    @Column(name = "filepath")
    @NotNull(message = "field required")
    private String filePath;

    @OneToOne(mappedBy = "video")
    private Lecture lecture;

    public Video() {

    }

    public Video(String filePath, Lecture lecture) {
        this.filePath = filePath;
        this.lecture = lecture;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public Lecture getLecture() {
        return lecture;
    }

    public void setLecture(Lecture lecture) {
        this.lecture = lecture;
    }

}
