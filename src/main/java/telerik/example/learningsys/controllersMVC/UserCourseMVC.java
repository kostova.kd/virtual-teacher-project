package telerik.example.learningsys.controllersMVC;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import telerik.example.learningsys.services.Interfaces.UserCourseService;
import telerik.example.learningsys.services.Interfaces.UserService;

@Controller
@RequestMapping
public class UserCourseMVC {
    private UserCourseService userCourseService;
    private UserService userService;

    @Autowired
    public UserCourseMVC(UserCourseService userCourseService, UserService userService) {
        this.userCourseService = userCourseService;
        this.userService = userService;
    }

    @GetMapping("/users/{userId}/courses")
    public String getUserCourses(@PathVariable int userId, Model model) {
        model.addAttribute("draftCourses", userCourseService.getTeacherDraftCourses(userId));
        model.addAttribute("readyCourses", userCourseService.getTeacherReadyCourses(userId));
        model.addAttribute("user", userService.getUserById(userId));
        model.addAttribute("enrolledCourses", userCourseService.getUserEnrolledCourses(userId));
        model.addAttribute("completedCourses", userCourseService.getUserCompletedCourses(userId));
        return "usercourses";
    }
}
