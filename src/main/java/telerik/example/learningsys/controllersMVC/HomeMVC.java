package telerik.example.learningsys.controllersMVC;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import telerik.example.learningsys.models.User;
import telerik.example.learningsys.services.Interfaces.CourseService;
import telerik.example.learningsys.services.Interfaces.TopicService;
import telerik.example.learningsys.services.Interfaces.UserService;

import javax.validation.Valid;
import java.security.Principal;

@Controller
@RequestMapping
public class HomeMVC {
    private PasswordEncoder passwordEncoder;
    private TopicService topicService;
    private UserService userService;

    @Autowired
    public HomeMVC(TopicService topicService, UserService userService, PasswordEncoder passwordEncoder) {
        this.topicService = topicService;
        this.userService = userService;
        this.passwordEncoder = passwordEncoder;
    }

    @GetMapping("/home")
    public String getHomePage(Model model, Principal principal) {
        if (principal != null) {
            String name = principal.getName();
            User user = userService.getUserByName(name);
            model.addAttribute("user", user);
        }
        model.addAttribute("topics", topicService.getAll());
        return "home";
    }

    @GetMapping("/")
    public String showHomePage() {
        return "redirect:/home";
    }

    @GetMapping("/register")
    public String showRegisterPage(Model model) {
        model.addAttribute("user", new User());
        return "register";
    }

    @PostMapping("/register")
    public String registerUser(@Valid @ModelAttribute User user,
                               BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("error", "Username/password can't be empty.");
            return "register";
        }
        user.setPassword(passwordEncoder.encode(user.getPassword()));

        try {
            userService.createUser(user, "ROLE_USER");
        } catch (Exception e) {
            model.addAttribute("error", "User already exists.");
            return "register";
        }
        return "redirect:/home";
    }

    @GetMapping("/login")
    public String showLogin() {
        return "login";
    }

}
