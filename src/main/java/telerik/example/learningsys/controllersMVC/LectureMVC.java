package telerik.example.learningsys.controllersMVC;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import telerik.example.learningsys.services.Interfaces.LectureService;

@Controller
@RequestMapping
public class LectureMVC {
    private LectureService lectureService;

    @Autowired
    public LectureMVC(LectureService lectureService) {
        this.lectureService = lectureService;
    }

    @GetMapping("/lectures/{lectureId}")
    public String getLecture(@PathVariable int lectureId, Model model) {
        model.addAttribute("lecture", lectureService.getLectureById(lectureId));
        return "user-lecture";
    }
}
