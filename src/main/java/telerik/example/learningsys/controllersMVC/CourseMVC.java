package telerik.example.learningsys.controllersMVC;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import telerik.example.learningsys.models.*;
import telerik.example.learningsys.services.Interfaces.*;

import java.security.Principal;

@Controller
@RequestMapping
public class CourseMVC {
    private static String STATIC_RESOURCES_DIR = System.getProperty("user.dir") + "/src/main/resources/static";
    private static String COURSES_UPLOAD_DIR = "/upload/";
    private static String ABSOLUTE_COURSES_UPLOAD_DIR = STATIC_RESOURCES_DIR + COURSES_UPLOAD_DIR;

    private CourseService courseService;
    private LectureService lectureService;
    private TopicService topicService;
    private UserService userService;
    private UserCourseService userCourseService;
    private UploadService uploadService;

    @Autowired
    public CourseMVC(CourseService courseService, LectureService lectureService, TopicService topicService,
                     UserService userService, UserCourseService userCourseService, UploadService uploadService) {
        this.courseService = courseService;
        this.lectureService = lectureService;
        this.topicService = topicService;
        this.userService = userService;
        this.userCourseService = userCourseService;
        this.uploadService = uploadService;
    }

    @GetMapping("/courses/{courseId}")
    public String getCourse(@PathVariable int courseId, Model model, Principal principal) {
        if (principal != null) {
            String name = principal.getName();
            int userId = userService.getUserByName(name).getId();
            UserCourse userCourse = userCourseService.getUserCourseByUserAndCourseId(userId, courseId);
            model.addAttribute("userCourse", userCourse);
            model.addAttribute("user", userService.getUserById(userId));
            model.addAttribute("userCourseCompleted", userCourseService.userCourseIsCompleted(userCourse));
            model.addAttribute("userCourseCompletedSolutions", userCourseService.getUserCourseCompletedSolutions(userCourse));
            model.addAttribute("userCourseInProgressLectures", userCourseService.getUserCourseInProgressLectures(userCourse));
            model.addAttribute("userCourseAverageGrade", userCourseService.getAverageGrade(userCourse));
            model.addAttribute("userCourseProgress", userCourseService.getProgress(userCourse));
        }
        model.addAttribute("course", courseService.getCourseById(courseId));
        return "user-course";
    }

    @GetMapping("/courses/{id}/edit")
    public String editCourse(@PathVariable int id, Model model, Principal principal) {
        String name = principal.getName();
        User user = userService.getUserByName(name);
        Course course = courseService.getCourseById(id);
        model.addAttribute("course", course);
        model.addAttribute("topics", topicService.getAll());
        model.addAttribute("lecture", new Lecture());
        model.addAttribute("user", user);
        model.addAttribute("assignment", new Assignment());
        return "edit-course";
    }

    @PostMapping("/courses/{id}/edit")
    public String editCourse(@RequestParam("file") MultipartFile file, @PathVariable int id, Course course, Principal principal) {
        String name = principal.getName();
        User user = userService.getUserByName(name);
        try {
            course.setTeacher(user);
            course.setImagePath(uploadService.writeFile(principal, file, ABSOLUTE_COURSES_UPLOAD_DIR, COURSES_UPLOAD_DIR));
            courseService.update(id, course);
        } catch (Exception e) {
            return "redirect:/courses/{id}";
        }
        return "redirect:/courses/{id}";
    }

    @PostMapping("/courses/{id}/lectures/new")
    public String addCourseLecture(@PathVariable int id,
                                   Principal principal,
                                   @RequestParam("lectureImage") MultipartFile lectureImage,
                                   @RequestParam("videoFile") MultipartFile videoFile,
                                   @RequestParam("assignmentFile") MultipartFile assignmentFile,
                                   @RequestParam String assignmentTitle,
                                   Lecture lecture,
                                   RedirectAttributes redirectAttributes) {
        try {
            Video video = uploadService.uploadVideo(principal, videoFile, lecture.getId());
            Assignment assignment = uploadService.uploadAssignment(principal, assignmentFile, lecture.getId(), assignmentTitle);
            Lecture updatedLecture = uploadService.addImageToLecture(principal, lectureImage, lecture);
            lectureService.addCourseLecture(id, updatedLecture, video, assignment);
        } catch (Exception e) {
            redirectAttributes.addFlashAttribute("lectureAddedException", "Failed to add lecture!");
            return "redirect:/courses/{id}/edit";
        }
        redirectAttributes.addFlashAttribute("lectureAdded", "Lecture successfully added!");
        return "redirect:/courses/{id}/edit";
    }

    @GetMapping("/courses/new")
    public String createCourse(Model model, Principal principal) {
        String name = principal.getName();
        User user = userService.getUserByName(name);
        model.addAttribute("course", new Course());
        model.addAttribute("topics", topicService.getAll());
        model.addAttribute("user", user);
        return "create-course";
    }

    @PostMapping("/courses/new")
    public String createCourse(@RequestParam("file") MultipartFile file, RedirectAttributes redirectAttributes,
                               @ModelAttribute Course course, Principal principal) {
        String name = principal.getName();
        User user = userService.getUserByName(name);
        try {
            course.setTeacher(user);
            course.setImagePath(uploadService.writeFile(principal, file, ABSOLUTE_COURSES_UPLOAD_DIR, COURSES_UPLOAD_DIR));
            course.setRating(-1);
            courseService.create(course);
        } catch (Exception e) {
            redirectAttributes.addFlashAttribute("courseCreatedException", "Course creation failed!");
            return "redirect:/home";
        }
        redirectAttributes.addFlashAttribute("courseCreated", "New course created!");
        return "redirect:/users/" + user.getId() + "/courses";
    }

    @PostMapping("/courses/{courseId}/enroll")
    public String enrollToCourse(@PathVariable int courseId, Principal principal, RedirectAttributes redirectAttributes) {
        try {
            String name = principal.getName();
            int userId = userService.getUserByName(name).getId();
            userService.enrollToCourse(userId, courseId);
        } catch (Exception e) {
            redirectAttributes.addFlashAttribute("enrollExceptionMessage", "Problem enrolling to course");
            return "redirect:/courses/{courseId}";
        }
        redirectAttributes.addFlashAttribute("enrollMessage", "Successfully enrolled to course!");
        return "redirect:/courses/{courseId}";
    }
}
