package telerik.example.learningsys.controllersMVC;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import telerik.example.learningsys.models.*;
import telerik.example.learningsys.services.Interfaces.*;

import java.security.Principal;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Controller
@RequestMapping
public class UserMVC {
    private static String STATIC_RESOURCES_DIR = System.getProperty("user.dir") + "/src/main/resources/static";
    private static String COURSES_UPLOAD_DIR = "/upload/";
    private static String ABSOLUTE_COURSES_UPLOAD_DIR = STATIC_RESOURCES_DIR + COURSES_UPLOAD_DIR;

    private AssignmentService assignmentService;
    private UserService userService;
    private SolutionService solutionService;
    private UploadService uploadService;
    private UserCourseService userCourseService;

    private PasswordEncoder passwordEncoder;

    @Autowired
    public UserMVC(UserService userService, SolutionService solutionService,
                   UploadService uploadService, UserCourseService userCourseService,
                   AssignmentService assignmentService, PasswordEncoder passwordEncoder) {
        this.userService = userService;
        this.solutionService = solutionService;
        this.uploadService = uploadService;
        this.userCourseService = userCourseService;
        this.passwordEncoder = passwordEncoder;
        this.assignmentService = assignmentService;
    }

    @GetMapping("/users/{id}")
    public String getUserById(Model model, @PathVariable int id) {
        model.addAttribute("user", userService.getUserById(id));
        return "userdetails";
    }

    @GetMapping("/users/new")
    public String showNewEmployeeForm(Model model) {
        model.addAttribute("user", new User());
        return "createuser";
    }

    @PostMapping("/users/new")
    public String createUser(@ModelAttribute User user,
                             @RequestParam String authority,
                             RedirectAttributes redirectAttributes) {
        String password = "{noop}" + user.getPassword();
        user.setPassword(password);
        try {
            userService.createUser(user, authority);
        } catch (IllegalArgumentException e) {
            redirectAttributes.addFlashAttribute("createUserExceptionMessage", e.getMessage());
            return "redirect:/users";
        }
        redirectAttributes.addFlashAttribute("createdUserMessage", "user created!");
        return "redirect:/users";
    }

    @GetMapping("/users/{id}/edit")
    public String showEditUserForm(Model model, @PathVariable int id) {
        model.addAttribute("user", userService.getUserById(id));
        model.addAttribute("authority", userService.getUserAuthorityById(id));
        return "edituser";
    }

    @PutMapping("/users/{id}/edit")
    public String editUser(@RequestParam(required = false) MultipartFile userPicture, @PathVariable int id, @ModelAttribute User user,
                           @RequestParam String authority, RedirectAttributes redirectAttributes, Principal principal) {
        try {
            if (userPicture.getSize() > 0) {
                user.setPicture(uploadService.writeFile(principal, userPicture, ABSOLUTE_COURSES_UPLOAD_DIR, COURSES_UPLOAD_DIR));
            }

            if (!user.getPicture().isEmpty() && userPicture.getSize() == 0) {
                user.setPicture(user.getPicture());
            }

            userService.updateUser(id, user, authority);
        } catch (Exception e) {
            redirectAttributes.addFlashAttribute("editUserExceptionMessage", e.getMessage());
            return "redirect:/home";
        }
        redirectAttributes.addFlashAttribute("updatedUserMessage", "user updated!");
        return "redirect:/home";
    }

    @GetMapping("/users/{id}/password")
    public String showEditPasswordForm(Model model, @PathVariable int id) {
        model.addAttribute("user", userService.getUserById(id));
        return "changepassword";
    }

    @PutMapping("/users/{id}/password")
    public String editUserPassword(@PathVariable int id, @RequestParam("password") String password,
                                   RedirectAttributes redirectAttributes) {
        try {
            String encodedPassword = passwordEncoder.encode(password);
            userService.updatePassword(id, encodedPassword);
        } catch (IllegalArgumentException e) {
            redirectAttributes.addFlashAttribute("editPasswordExceptionMessage", "Password update failed!");
            return "redirect:/home";
        }
        redirectAttributes.addFlashAttribute("editPasswordMessage", "Password updated!");
        return "redirect:/home";
    }

    @RequestMapping(value="/users/{id}/delete",method={RequestMethod.GET, RequestMethod.DELETE})
    public String deleteUser(@PathVariable int id) {
        userService.deleteUser(id);
        return "redirect:/users";
    }

    @PostMapping("/ratings/{courseId}")
    public String addRating(@PathVariable int courseId, Principal principal,
                            @RequestParam("rating") int rating, RedirectAttributes redirectAttributes) {
        try {
            userService.addRating(courseId, principal, rating);
        } catch (IllegalArgumentException e) {
            redirectAttributes.addFlashAttribute("addRatingExceptionMessage", "Add Rating forbidden! You must complete course.");
            return "redirect:/home"; }
        redirectAttributes.addFlashAttribute("addRatingMessage", "Rating added!");
        return "redirect:/home";
    }

    @GetMapping("/users")
    public String listUsers(Model model,
            @RequestParam("page") Optional<Integer> page,
            @RequestParam("size") Optional<Integer> size,
            Principal principal) {
        if (principal != null) {
            String name = principal.getName();
            User user = userService.getUserByName(name);
            model.addAttribute("user", user);
        }
        int currentPage = page.orElse(1);
        int pageSize = size.orElse(3);
        Page<User> userPage = userService.findPaginatedUsers(PageRequest.of(currentPage - 1, pageSize));
        model.addAttribute("userPage", userPage);
        int totalPages = userPage.getTotalPages();
        if (totalPages > 0) {
            List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages)
                    .boxed()
                    .collect(Collectors.toList());
            model.addAttribute("pageNumbers", pageNumbers);
        }
        return "users";
    }

    private String showUploadUnsuccessfulMessage(RedirectAttributes redirectAttributes) {
        redirectAttributes.addFlashAttribute("uploadExceptionMessage", "Upload unsuccessful.");
        return "redirect:/home";
    }

    @PostMapping("/uploaded-solutions/assignments/{assignmentId}")
    public String uploadSolution(@RequestParam("file") MultipartFile file, RedirectAttributes redirectAttributes,
                                 Principal principal, @PathVariable int assignmentId) {
        try {
            int courseId = assignmentService.getAssignmentById(assignmentId).getLecture().getCourse().getId();
            uploadService.uploadSolution(principal, file, assignmentId);
            redirectAttributes.addFlashAttribute("successfulUploadMessage", "You successfully uploaded the file");
            return "redirect:/courses/" + courseId;
        } catch (Exception e) {
            return showUploadUnsuccessfulMessage(redirectAttributes);
        }
    }

    @GetMapping("/solutions")
    public String listSolutions(Model model, Principal principal) {
        if (principal != null) {
            String name = principal.getName();
            User user = userService.getUserByName(name);
            model.addAttribute("user", user);
            model.addAttribute("solutions", solutionService.getSolutionsByAssignmentAndUserId(user.getId()));
        }
        return "assessmentpanel";
    }

    @PostMapping("/solutions/assessments/{solutionId}")
    public String assessSolution(RedirectAttributes redirectAttributes, @PathVariable int solutionId,
                                 @RequestParam("grade") int grade) {
        try {
            Solution solution = solutionService.getById(solutionId);
            solution.setGrade(grade);
            int userCourseId = solution.getUserCourse().getId();
            solutionService.update(solutionId, solution);
            userCourseService.setAverageGrade(userCourseId);
            redirectAttributes.addFlashAttribute("addAssessmentMessage", "Solution assessed!");
            return "redirect:/solutions";
        } catch (Exception e) {
            redirectAttributes.addFlashAttribute("addAssessmentExceptionMessage", "Assessment failed!");
            return "redirect:/solutions";
        }
    }
}