package telerik.example.learningsys.services;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import telerik.example.learningsys.models.*;
import telerik.example.learningsys.repositories.interfaces.UserCourseRepository;
import telerik.example.learningsys.repositories.interfaces.UserRepository;
import telerik.example.learningsys.services.Interfaces.SolutionService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RunWith(MockitoJUnitRunner.Silent.class)
public class UserCourseServiceImplTests {

    @Mock
    UserCourseRepository mockUserCourseRepository;

    @Mock
    UserRepository mockUserRepository;

    @Mock
    SolutionServiceImpl mockSolutionService;

    @InjectMocks
    UserCourseServiceImpl userCourseService;

    private Course mockCourse = new Course();
    private User mockUser = new User();
    private UserCourse mockUserCourse = new UserCourse();

    @Test
    public void getUserCourseById_Should_Return_Correct_UserCourse_When_Exists() {
        Mockito.when(mockUserCourseRepository.getUserCourseById(1))
                .thenReturn(mockUserCourse);
        UserCourse userCourse = userCourseService.getUserCourseById(1);
        Assert.assertEquals(mockUserCourse, userCourse);
    }

    @Test(expected = IllegalArgumentException.class)
    public void getUserCourseById_Should_Return_Correct_UserCourse_Doesnt_Exist() {
        Mockito.when(mockUserCourseRepository.getUserCourseById(1))
                .thenReturn(null);
        userCourseService.getUserCourseById(1);
    }

    @Test
    public void updateUserCourse_Succeeds() {
        UserCourse userCourse = new UserCourse(mockUser, mockCourse, "enrolled", -1);
        userCourseService.updateUserCourse(userCourse);
        Mockito.verify(mockUserCourseRepository, Mockito.times(1)).updateUserCourse(userCourse);
    }

    @Test(expected = IllegalArgumentException.class)
    public void updateUserCourse_Fails_With_Null() {
        userCourseService.updateUserCourse(null);
        Mockito.verify(mockUserCourseRepository, Mockito.times(1)).updateUserCourse(mockUserCourse);
    }

    @Test(expected = IllegalArgumentException.class)
    public void updateUserCourse_Fails_With_Fields() {
        userCourseService.updateUserCourse(mockUserCourse);
        Mockito.verify(mockUserCourseRepository, Mockito.times(1)).updateUserCourse(mockUserCourse);
    }

    @Test
    public void getUserCourseByUserAndCourseId_Succeeds() {
        Mockito.when(mockUserCourseRepository.getUserCourseByUserAndCourseId(1, 1))
                .thenReturn(mockUserCourse);
        UserCourse userCourse = userCourseService.getUserCourseByUserAndCourseId(1, 1);
        Assert.assertEquals(mockUserCourse, userCourse);
    }

    @Test
    public void getUserEnrolledCourses_Succeeds() {
        Mockito.when(mockUserCourseRepository.getUserCoursesByStatus(1, "enrolled"))
                .thenReturn(Arrays.asList(
                        mockUserCourse
                ));
        List<UserCourse> userCourses = userCourseService.getUserEnrolledCourses(1);
        Assert.assertEquals(1, userCourses.size());
    }

    @Test
    public void getUserCompletedCourses_Succeeds() {
        Mockito.when(mockUserCourseRepository.getUserCoursesByStatus(1, "completed"))
                .thenReturn(Arrays.asList(
                        mockUserCourse
                ));
        List<UserCourse> userCourses = userCourseService.getUserCompletedCourses(1);
        Assert.assertEquals(1, userCourses.size());
    }

    @Test
    public void userCourseIsCompleted_Returns_False_When_Null() {
        boolean completed = userCourseService.userCourseIsCompleted(null);
        Assert.assertFalse(completed);
    }

    @Test
    public void userCourseIsCompleted_Returns_True_When_Completed() {
        UserCourse userCourse = new UserCourse();
        userCourse.setStatus("completed");
        boolean completed = userCourseService.userCourseIsCompleted(userCourse);
        Assert.assertTrue(completed);
    }

    @Test
    public void userCourseIsCompleted_Returns_True_When_All_Lectures_Solved() {
        Solution mockSolutionFirst = new Solution();
        Solution mockSolutionSecond = new Solution();
        mockSolutionFirst.setGrade(5);
        mockSolutionSecond.setGrade(5);
        List<Solution> mockSolutions = Arrays.asList(mockSolutionFirst, mockSolutionSecond);
        List<Lecture> mockLectures = Arrays.asList(new Lecture(), new Lecture());
        mockUserCourse.setSolutions(mockSolutions);
        mockUserCourse.setStatus("enrolled");
        mockUserCourse.setCourse(mockCourse);
        mockUserCourse.getCourse().setLectures(mockLectures);
        Mockito.when(userCourseService.getUserCourseCompletedSolutions(mockUserCourse))
                .thenReturn(mockSolutions);
        boolean completed = userCourseService.userCourseIsCompleted(mockUserCourse);
        Assert.assertTrue(completed);
    }

    @Test
    public void getTeacherDraftCourses_Succeeds() {
        Course mockCourseFirst = new Course();
        Course mockCourseSecond = new Course();
        mockCourseFirst.setLectures(new ArrayList<>());
        mockCourseSecond.setLectures(new ArrayList<>());

        User user = new User();
        user.setTeacherCourses(Arrays.asList(mockCourseFirst, mockCourseSecond));
        Mockito.when(mockUserRepository.getUserById(1))
                .thenReturn(user);

        List<Course> teacherDraftCourses = userCourseService.getTeacherDraftCourses(1);
        Assert.assertEquals(2, teacherDraftCourses.size());

    }

    @Test
    public void getTeacherReadyCourses_Succeeds() {
        Course mockCourseFirst = new Course();
        Course mockCourseSecond = new Course();
        List<Lecture> mockLectures = Arrays.asList(new Lecture());
        mockCourseFirst.setLectures(mockLectures);
        mockCourseSecond.setLectures(mockLectures);

        User user = new User();
        user.setTeacherCourses(Arrays.asList(mockCourseFirst, mockCourseSecond));
        Mockito.when(mockUserRepository.getUserById(1))
                .thenReturn(user);

        List<Course> teacherDraftCourses = userCourseService.getTeacherReadyCourses(1);
        Assert.assertEquals(2, teacherDraftCourses.size());

    }

    @Test
    public void setAverageGrade_Updates_User() {
        Solution mockSolutionFirst = new Solution();
        Solution mockSolutionSecond = new Solution();
        mockSolutionFirst.setGrade(5);
        mockSolutionSecond.setGrade(5);
        Mockito.when(mockSolutionService.getSolutionsByUserCourseId(1))
                .thenReturn(Arrays.asList(
                        mockSolutionFirst, mockSolutionSecond
                ));
        Mockito.when(mockUserCourseRepository.getUserCourseById(1))
                .thenReturn(mockUserCourse);
        userCourseService.setAverageGrade(1);
        Mockito.verify(mockUserCourseRepository, Mockito.times(1)).updateUserCourse(mockUserCourse);
    }

    @Test
    public void getUserCourseInProgressLectures_Returns_Null_With_Null() {
        List<Lecture> lectures = userCourseService.getUserCourseInProgressLectures(null);
        Assert.assertNull(lectures);
    }

    @Test
    public void getUserCourseInProgressLectures_Returns_Null_When_Solutions_Null() {
        Solution mockSolutionFirst = new Solution();
        Solution mockSolutionSecond = new Solution();
        mockSolutionFirst.setGrade(-1);
        mockSolutionSecond.setGrade(-1);
        UserCourse userCourse = new UserCourse();
        Course course = new Course();
        List<Lecture> mockLectures = Arrays.asList(new Lecture(), new Lecture());
        course.setLectures(mockLectures);
        userCourse.setCourse(course);
        userCourse.setSolutions(Arrays.asList(mockSolutionFirst, mockSolutionSecond));
        Mockito.when(userCourseService.getUserCourseCompletedSolutions(userCourse))
                .thenReturn(null);
        List<Lecture> lectures = userCourseService.getUserCourseInProgressLectures(userCourse);
        Assert.assertEquals(2, lectures.size());
    }

    @Test
    public void getAverageGrade_Returns_Null() {
        String average = userCourseService.getAverageGrade(null);
        Assert.assertNull(average);
    }

    @Test
    public void getAverageGrade_Returns_Correct_Decimals() {
        UserCourse userCourse = new UserCourse();
        userCourse.setAverageGrade(3.3333333);
        String average = userCourseService.getAverageGrade(userCourse);
        Assert.assertEquals("3.3", average);
    }

    @Test
    public void getProgress_Returns_Zero() {
        int progress = userCourseService.getProgress(null);
        Assert.assertEquals(0, progress);
    }
}
