package telerik.example.learningsys.services;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import telerik.example.learningsys.models.Video;
import telerik.example.learningsys.repositories.interfaces.VideoRepository;

import java.util.Arrays;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class VideoServiceImplTests {
    @Mock
    private VideoRepository mockVideoRepository;

    @InjectMocks
    VideoServiceImpl videoService;

    Video mockVideo = new Video();

    @Test
    public void getAll_Should_Return_A_List_Of_Videos() {

        //Arrange
        Mockito.when(mockVideoRepository.getAll())
                .thenReturn(Arrays.asList(mockVideo));
        //Act
        List<Video> videos = videoService.getAll();

        //Assert
        Assert.assertEquals(1, videos.size());
    }

    @Test
    public void create_Should_Call_Repository_Create() {
        videoService.create(mockVideo);
        Mockito.verify(mockVideoRepository, Mockito.times(1)).create(mockVideo);
    }

    @Test
    public void update_Should_Call_Repository_Update() {
        videoService.update(1, mockVideo);
        Mockito.verify(mockVideoRepository, Mockito.times(1)).update(mockVideo);
    }

    @Test
    public void delete_Should_Call_Repository_Delete() {
        Mockito.when(mockVideoRepository.getVideoById(1))
                .thenReturn(mockVideo);
        videoService.delete(1);
        Mockito.verify(mockVideoRepository, Mockito.times(1)).delete(mockVideo);
    }
}
