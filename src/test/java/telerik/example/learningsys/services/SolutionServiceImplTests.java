package telerik.example.learningsys.services;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import telerik.example.learningsys.models.*;
import telerik.example.learningsys.repositories.interfaces.SolutionRepository;
import telerik.example.learningsys.repositories.interfaces.VideoRepository;

import java.util.Arrays;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class SolutionServiceImplTests {
    @Mock
    private SolutionRepository mockSolutionRepository;

    @InjectMocks
    SolutionServiceImpl solutionService;

    Solution mockSolution = new Solution();
    Assignment mockAssignment = new Assignment();
    User mockUser = new User();
    UserCourse mockUserCourse = new UserCourse();

    @Test
    public void getAll_Should_Return_A_List_Of_Solutions() {

        //Arrange
        Mockito.when(mockSolutionRepository.getAll())
                .thenReturn(Arrays.asList(mockSolution));
        //Act
        List<Solution> videos = solutionService.getAll();

        //Assert
        Assert.assertEquals(1, videos.size());
    }

    @Test
    public void getSolutionsByAssignmentId_Should_Succeed() {
        Mockito.when(mockSolutionRepository.getSolutionsByAssignmentAndUserId(1))
                .thenReturn(Arrays.asList(mockSolution));
        List<Solution> solutions = solutionService.getSolutionsByAssignmentAndUserId(1);
        Assert.assertEquals(1, solutions.size());
    }

    @Test
    public void getSolutionById_Should_Succeed() {
        Mockito.when(mockSolutionRepository.getSolutionById(1))
                .thenReturn(mockSolution);
        Solution solution = solutionService.getById(1);
        Assert.assertEquals(mockSolution, solution);
    }

    @Test
    public void getSolutionsByUserCourseId_Should_Succeed() {
        Mockito.when(mockSolutionRepository.getSolutionsByUserCourseId(1))
                .thenReturn(Arrays.asList(mockSolution));
        List<Solution> solutions = solutionService.getSolutionsByUserCourseId(1);
        Assert.assertEquals(1, solutions.size());
    }

    @Test
    public void create_Should_Call_Repository_Create() {
        mockSolution.setFilePath("/path/");
        mockSolution.setAssignment(mockAssignment);
        mockSolution.setUser(mockUser);
        solutionService.create(mockSolution);
        Mockito.verify(mockSolutionRepository, Mockito.times(1)).create(mockSolution);
    }

    @Test(expected = IllegalArgumentException.class)
    public void create_Should_Throw_With_Null() {
        solutionService.create(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void create_Should_Throw_With_Null_Objects() {
        solutionService.create(mockSolution);
    }

    @Test
    public void update_Should_Call_Repository_Update() {
        solutionService.update(1, mockSolution);
        Mockito.verify(mockSolutionRepository, Mockito.times(1)).update(mockSolution);
    }

    @Test
    public void delete_Should_Call_Repository_Delete() {
        Mockito.when(mockSolutionRepository.getSolutionById(1))
                .thenReturn(mockSolution);
        solutionService.delete(1);
        Mockito.verify(mockSolutionRepository, Mockito.times(1)).delete(mockSolution);
    }
}
