package telerik.example.learningsys.services;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import telerik.example.learningsys.models.*;
import telerik.example.learningsys.repositories.interfaces.AssignmentRepository;

import java.util.Arrays;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class AssignmentServiceImplTests {
    @Mock
    private AssignmentRepository mockAssignmentRepository;

    @InjectMocks
    AssignmentServiceImpl assignmentService;

    Assignment mockAssignment = new Assignment();

    @Test
    public void getAll_Should_Return_A_List_Of_Assignments() {

        //Arrange
        Mockito.when(mockAssignmentRepository.getAll())
                .thenReturn(Arrays.asList(mockAssignment));
        //Act
        List<Assignment> ratings = assignmentService.getAll();

        //Assert
        Assert.assertEquals(1, ratings.size());
    }

    @Test
    public void getAssignmentById_Should_Return_Assignment_When_Id_Matches() {
        Mockito.when(mockAssignmentRepository.getAssignmentById(1))
                .thenReturn(mockAssignment);
        Assignment assignment = assignmentService.getAssignmentById(1);
        Assert.assertEquals(mockAssignment, assignment);
    }

    @Test
    public void create_Should_Call_Repository_Create() {
        assignmentService.create(mockAssignment);
        Mockito.verify(mockAssignmentRepository, Mockito.times(1)).create(mockAssignment);
    }

    @Test
    public void update_Should_Call_Repository_Update() {
        assignmentService.update(1, mockAssignment);
        Mockito.verify(mockAssignmentRepository, Mockito.times(1)).update(mockAssignment);
    }

    @Test
    public void delete_Should_Call_Repository_Delete() {
        Mockito.when(mockAssignmentRepository.getAssignmentById(1))
                .thenReturn(mockAssignment);
        assignmentService.delete(1);
        Mockito.verify(mockAssignmentRepository, Mockito.times(1)).delete(mockAssignment);
    }
}
