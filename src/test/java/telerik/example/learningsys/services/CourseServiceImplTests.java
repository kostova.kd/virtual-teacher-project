package telerik.example.learningsys.services;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import telerik.example.learningsys.models.Course;
import telerik.example.learningsys.models.Lecture;
import telerik.example.learningsys.models.Rating;
import telerik.example.learningsys.models.User;
import telerik.example.learningsys.repositories.interfaces.CourseRepository;
import telerik.example.learningsys.repositories.interfaces.RatingRepository;

import java.util.Arrays;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class CourseServiceImplTests {

    @Mock
    CourseRepository mockCourseRepository;

    @Mock
    RatingRepository mockRatingRepository;

    @InjectMocks
    CourseServiceImpl courseService;

    User mockUser = new User();
    Course mockCourse = new Course();
    Lecture mockLecture = new Lecture();

    Rating mockRatingFirst = new Rating(1, 4, mockUser, mockCourse);
    Rating mockRatingSecond = new Rating(1, 5, mockUser, mockCourse);

    @Test
    public void getAll_Should_Return_All_Courses() {
        Mockito.when(mockCourseRepository.getAll())
                .thenReturn(Arrays.asList(
                        mockCourse
                ));
        List<Course> courses = courseService.getAll();
        Assert.assertEquals(1, courses.size());
    }

    @Test
    public void getCourseById_Should_Return_Correct_Course_When_Exists() {
        Mockito.when(mockCourseRepository.getCourseById(1))
                .thenReturn(mockCourse);
        Course course = courseService.getCourseById(1);
        Assert.assertEquals(mockCourse, course);
    }

    @Test(expected = IllegalArgumentException.class)
    public void getCourseById_Should_Return_Correct_Course_When_Doesnt_Exist() {
        Mockito.when(mockCourseRepository.getCourseById(1))
                .thenReturn(null);
        courseService.getCourseById(1);
    }

    @Test
    public void createCourse_Should_Succeed() {
        courseService.create(mockCourse);
        Mockito.verify(mockCourseRepository, Mockito.times(1)).create(mockCourse);
    }

    @Test
    public void updateCourse_Should_Succeed() {
        courseService.update(1, mockCourse);
        Mockito.verify(mockCourseRepository, Mockito.times(1)).update(mockCourse);
    }

    @Test
    public void deleteCourse_Should_Succeed() {
        Mockito.when(mockCourseRepository.getCourseById(1))
                .thenReturn(mockCourse);
        courseService.delete(1);
        Mockito.verify(mockCourseRepository, Mockito.times(1)).delete(mockCourse);
    }

    @Test
    public void getCourseLectures_Should_Return_Course_Lectures() {
        Mockito.when(mockCourseRepository.getCourseLectures(1))
                .thenReturn(Arrays.asList(
                        mockLecture
                ));
        List<Lecture> lectures = courseService.getCourseLectures(1);
        Assert.assertEquals(1, lectures.size());
    }

    @Test
    public void getAverageGrade_Should_Return_Correct_Double() {
        Mockito.when(mockRatingRepository.getRatingsOfCourse(1))
                .thenReturn(Arrays.asList(
                        mockRatingFirst, mockRatingSecond
                ));
        double averageGrade = courseService.getAverageRating(1);
        Assert.assertEquals(4.5, averageGrade, 0);
    }

}
