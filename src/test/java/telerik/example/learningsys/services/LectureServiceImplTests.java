package telerik.example.learningsys.services;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import telerik.example.learningsys.models.Assignment;
import telerik.example.learningsys.models.Course;
import telerik.example.learningsys.models.Lecture;
import telerik.example.learningsys.models.Video;
import telerik.example.learningsys.repositories.interfaces.CourseRepository;
import telerik.example.learningsys.repositories.interfaces.LectureRepository;

import java.util.Arrays;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class LectureServiceImplTests {

    @Mock
    private LectureRepository mockLectureRepository;

    @Mock
    private CourseRepository mockCourseRepository;

    @InjectMocks
    LectureServiceImpl lectureService;

    Lecture mockLecture = new Lecture();
    Course mockCourse = new Course();
    Video mockVideo = new Video();
    Assignment mockAssignment = new Assignment();

    @Test
    public void getAll_Should_Return_A_List_Of_Lectures() {

        //Arrange
        Mockito.when(mockLectureRepository.getAll())
                .thenReturn(Arrays.asList(mockLecture));
        //Act
        List<Lecture> lectures = lectureService.getAll();

        //Assert
        Assert.assertEquals(1, lectures.size());
    }

    @Test
    public void getLectureById_Should_Return_Lecture_When_Id_Matches() {
        Mockito.when(mockLectureRepository.getLectureById(1))
                .thenReturn(mockLecture);
        Lecture lecture = lectureService.getLectureById(1);
        Assert.assertEquals(mockLecture, lecture);
    }

    @Test
    public void create_Should_Call_Repository_Create() {
        lectureService.create(mockLecture);
        Mockito.verify(mockLectureRepository, Mockito.times(1)).create(mockLecture);
    }

    @Test
    public void update_Should_Call_Repository_Update() {
        lectureService.update(1, mockLecture);
        Mockito.verify(mockLectureRepository, Mockito.times(1)).update(mockLecture);
    }

    @Test
    public void delete_Should_Call_Repository_Delete() {
        Mockito.when(mockLectureRepository.getLectureById(1))
                .thenReturn(mockLecture);
        lectureService.delete(1);
        Mockito.verify(mockLectureRepository, Mockito.times(1)).delete(mockLecture);
    }

    @Test(expected = IllegalArgumentException.class)
    public void addCourseLecture_Throws_When_Not_Valid() {
        Mockito.when(mockCourseRepository.getCourseById(1))
                .thenThrow(new IllegalArgumentException());
        lectureService.addCourseLecture(1, mockLecture, mockVideo, mockAssignment);
        Mockito.verify(mockLectureRepository, Mockito.times(1)).create(mockLecture);
    }

    @Test
    public void addCourseLecture_Should_Add_Lecture_To_Course() {
        Mockito.when(mockCourseRepository.getCourseById(1))
                .thenReturn(mockCourse);
        lectureService.addCourseLecture(1, mockLecture, mockVideo, mockAssignment);
        Mockito.verify(mockLectureRepository, Mockito.times(1)).create(mockLecture);
    }
}
