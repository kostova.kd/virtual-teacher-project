package telerik.example.learningsys.services;

import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;
import telerik.example.learningsys.models.*;
import telerik.example.learningsys.repositories.*;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import telerik.example.learningsys.repositories.interfaces.UserRepository;
import telerik.example.learningsys.services.Interfaces.AssignmentService;
import telerik.example.learningsys.services.Interfaces.SolutionService;

import javax.security.auth.Subject;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.security.Principal;
import java.util.Arrays;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;

@RunWith(MockitoJUnitRunner.class)
public class UploadServiceImplTests {
    private static String STATIC_RESOURCES_DIR = System.getProperty("user.dir") + "/src/main/resources/static";
    private static String COURSES_UPLOAD_DIR = "/upload/";
    private static String ABSOLUTE_COURSES_UPLOAD_DIR = STATIC_RESOURCES_DIR + COURSES_UPLOAD_DIR;

    @Mock
    private UserRepository mockUserRepository;

    @Mock
    private AssignmentService mockAssignmentService;

    @Mock
    private SolutionService mockSolutionService;

    @Mock
    private RatingRepositoryImpl mockRatingRepository;

    @Mock
    private SolutionRepositoryImpl mockSolutionRepository;

    @Mock
    private UserCourseRepositoryImpl mockUserCourseRepository;

    @Mock
    private TopicRepositoryImpl mockTopicRepository;

    @Mock
    private CourseRepositoryImpl mockCourseRepository;

    @Mock
    private LectureRepositoryImpl mockLectureRepository;

    @Mock
    private VideoRepositoryImpl mockVideoRepository;

    @Mock
    private AssignmentRepositoryImpl mockAssignmentRepository;

    @Mock
    Principal principal;

    @InjectMocks
    UploadServiceImpl uploadService;

    User mockUser = new User("Nasko", "$2a$10$J5154R302KL0Cm0CrtHqCeQ0pC3g5aCfApnF0bg1seShMmcJZ4Mzq", "nasko@ABV.BG", "PNP.PNG");
    User mockTeacher = new User("pesho", "$2a$10$J5154R302KL0Cm0CrtHqCeQ0pC3g5aCfApnF0bg1seShMmcJZ4Mzq", "pesho@ABV.BG", "pinko.PNG");
    Topic mockTopic = new Topic("JAVA");
    Course mockCourse = new Course("OOP", mockTopic, "ddddfs", "rthrwge", "/fcef/eev", mockTeacher);
    UserCourse mockUserCourse = new UserCourse(mockUser, mockCourse, "enrolled", -1);
    Assignment assignment = new Assignment();

    MockMultipartFile file = new MockMultipartFile("files", "filename.txt", "text/plain", "hello".getBytes(StandardCharsets.UTF_8));


    class PrincipalStub implements Principal {

        @Override
        public String getName() {
            return "Vasko";
        }

        @Override
        public boolean implies(Subject subject) {
            return false;
        }
    }

    @Test(expected = IllegalArgumentException.class)
    public void uploadSolution_Fails() {
        uploadService.uploadSolution(principal, file, assignment.getId());
    }
}
