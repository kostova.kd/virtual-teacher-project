package telerik.example.learningsys.services;

import telerik.example.learningsys.models.*;
import telerik.example.learningsys.repositories.*;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceImplTests {

    @Mock
    private UserRepositoryImpl mockUserRepository;

    @Mock
    private RatingRepositoryImpl mockRatingRepository;

    @Mock
    private SolutionRepositoryImpl mockSolutionRepository;

    @Mock
    private UserCourseRepositoryImpl mockUserCourseRepository;

    @Mock
    private TopicRepositoryImpl mockTopicRepository;

    @Mock
    private CourseRepositoryImpl mockCourseRepository;

    @Mock
    private LectureRepositoryImpl mockLectureRepository;

    @Mock
    private VideoRepositoryImpl mockVideoRepository;

    @Mock
    private AssignmentRepositoryImpl mockAssignmentRepository;

    @InjectMocks
    UserServiceImpl userService;

    //Country mockCountry = new Country("Bulgaria");
    String authority = "ADMIN";
    //Style mockStyle = new Style("Dark IPA");
    //Brewery mockBrewery = new Brewery("Kamenitza Brewery");

    User mockUser = new User("Nasko", "$2a$10$J5154R302KL0Cm0CrtHqCeQ0pC3g5aCfApnF0bg1seShMmcJZ4Mzq", "nasko@ABV.BG", "PNP.PNG");
    User mockTeacher = new User("pesho", "$2a$10$J5154R302KL0Cm0CrtHqCeQ0pC3g5aCfApnF0bg1seShMmcJZ4Mzq", "pesho@ABV.BG", "pinko.PNG");
    Topic mockTopic = new Topic("JAVA");
    Course mockCourse = new Course("OOP", mockTopic, "ddddfs", "rthrwge", "/fcef/eev", mockTeacher);
    UserCourse mockUserCourse = new UserCourse(mockUser, mockCourse, "enrolled", -1);

    @Test
    public void listUsers_Should_Return_A_List_Of_Users() {

        //Arrange
        Mockito.when(mockUserRepository.listUsers())
                .thenReturn(Arrays.asList(
                        new User("Vasko", "123456", "picture.jpg", "vasko@abv.bg")));
        //Act
        List<User> users = userService.listUsers();

        //Assert
        Assert.assertEquals(1, users.size());
    }

    @Test
    public void getUserById_Should_Return_A_User_When_Id_Matches() {
        Mockito.when(mockUserRepository.getUserById(1))
                .thenReturn(new User("Nasko", "123456", "picture.jpg", "nasko@abv.bg"));
        User user = userService.getUserById(1);
        Assert.assertEquals("Nasko", user.getUsername());
    }

    @Test(expected = IllegalArgumentException.class)
    public void getUserById_Should_Return_An_Exception_When_Id_Doesnt_Match() {
        User user = userService.getUserById(2);
    }

    @Test
    public void getUserByName_Should_Return_A_User_When_Name_Matches() {
        Mockito.when(mockUserRepository.getUserByName("Nasko"))
                .thenReturn(new User("Nasko", "123456", "picture.jpg", "nasko@abv.bg"));
        User user = userService.getUserByName("Nasko");
        Assert.assertEquals("Nasko", user.getUsername());
    }

    @Test(expected = IllegalArgumentException.class)
    public void getUserByName_Should_Return_An_Exception_When_Name_Doesnt_Match() {
        User user = userService.getUserByName("ZZZZ");
    }

    @Test
    public void create_Should_Call_Repository_Create() {
        User user = new User("Pesho", "123456", "picture.jpg", "pesho@abv.bg");
        userService.createUser(user, authority);
        Mockito.verify(mockUserRepository, Mockito.times(1)).createUser(user, authority);
    }

    @Test
    public void update_Should_Call_Repository_Update() {
        User user = new User("Ivan", "12346", "picture.jpg", "ivan@abv.bg");
        userService.updateUser(1, user, authority);
        Mockito.verify(mockUserRepository, Mockito.times(1)).updateUser(1, user, authority);
    }

    @Test
    public void update_Should_Call_Repository_Update_Password() {
        String pass = "12346";
        userService.updatePassword(1, pass);
        Mockito.verify(mockUserRepository, Mockito.times(1)).updatePassword(1, pass);
    }

    @Test
    public void delete_Should_Call_Repository_Delete() {
        userService.deleteUser(1);
        Mockito.verify(mockUserRepository, Mockito.times(1)).deleteUser(1);
    }


/*

    @Test
    public void enrollToCourse_Should_Call_The_User_repository(){
        Mockito.when(mockUserRepository.getUserById(1))
                .thenReturn(mockUser);
        Mockito.when(mockUserRepository.getCoursesList(1))
                .thenReturn(Arrays.asList(mockCourse));
        Mockito.when(mockUserCourseRepository.getUserCourseByUserAndCourseId(1,1))
                .thenReturn(mockUserCourse);
        userService.enrollToCourse(1, 1);
        //Mockito.verify(mockUserRepository, Mockito.times(1)).enrollToCourse(1, 1, "enrolled", -1);
        Assert.assertEquals(mockUserCourse, mockUserCourseRepository.getUserCourseByUserAndCourseId(1,1));

    }

/*

    @Test
    public void addToDrankList_Should_Call_The_User_repository(){
        Mockito.when(mockUserRepository.getDrankList(1))
                .thenReturn(Arrays.asList(mockBeer));

        userService.addToDrankList(1, 1);

        Mockito.verify(mockUserRepository, Mockito.times(1)).addToDrankList(1, 1);
    }

     */




}