package telerik.example.learningsys.services;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import telerik.example.learningsys.models.Topic;
import telerik.example.learningsys.repositories.interfaces.TopicRepository;

import java.util.Arrays;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class TopicServiceImplTests {
    @Mock
    private TopicRepository mockTopicRepository;

    @InjectMocks
    TopicServiceImpl topicService;

    Topic mockTopic = new Topic();

    @Test
    public void getAll_Should_Return_A_List_Of_Topics() {

        //Arrange
        Mockito.when(mockTopicRepository.getAll())
                .thenReturn(Arrays.asList(mockTopic));
        //Act
        List<Topic> ratings = topicService.getAll();

        //Assert
        Assert.assertEquals(1, ratings.size());
    }

    @Test
    public void create_Should_Call_Repository_Create() {
        topicService.create(mockTopic);
        Mockito.verify(mockTopicRepository, Mockito.times(1)).create(mockTopic);
    }

    @Test
    public void update_Should_Call_Repository_Update() {
        topicService.update(1, mockTopic);
        Mockito.verify(mockTopicRepository, Mockito.times(1)).update(mockTopic);
    }

    @Test
    public void delete_Should_Call_Repository_Delete() {
        Mockito.when(mockTopicRepository.getTopicById(1))
                .thenReturn(mockTopic);
        topicService.delete(1);
        Mockito.verify(mockTopicRepository, Mockito.times(1)).delete(mockTopic);
    }
}
