package telerik.example.learningsys.services;

import telerik.example.learningsys.models.*;
import telerik.example.learningsys.repositories.*;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class RatingServiceImplTests {

    @Mock
    private RatingRepositoryImpl mockRatingRepository;

    @InjectMocks
    RatingServiceImpl ratingService;

    User mockUser = new User("Nasko", "$2a$10$J5154R302KL0Cm0CrtHqCeQ0pC3g5aCfApnF0bg1seShMmcJZ4Mzq", "nasko@ABV.BG", "PNP.PNG");
    User mockTeacher = new User("pesho", "$2a$10$J5154R302KL0Cm0CrtHqCeQ0pC3g5aCfApnF0bg1seShMmcJZ4Mzq", "pesho@ABV.BG", "pinko.PNG");
    Topic mockTopic = new Topic("JAVA");
    Course mockCourse = new Course("OOP", mockTopic, "ddddfs", "rthrwge", "/fcef/eev", mockTeacher);
    Rating mockRating = new Rating(2, mockUser, mockCourse);

    @Test
    public void getAll_Should_Return_A_List_Of_Ratings() {

        //Arrange
        Mockito.when(mockRatingRepository.getAll())
                .thenReturn(Arrays.asList(mockRating));
        //Act
        List<Rating> ratings = ratingService.getAll();

        //Assert
        Assert.assertEquals(1, ratings.size());
    }

    @Test
    public void getRatingById_Should_Return_A_Rating_When_Id_Matches() {
        Mockito.when(mockRatingRepository.getRatingsOfCourse(1))
                .thenReturn(Arrays.asList(mockRating));
        List<Rating> ratings = ratingService.getRatingsOfCourse(1);
        Assert.assertEquals(1, ratings.size());
    }

    @Test
    public void create_Should_Call_Repository_Create() {
        Rating mockRating = new Rating(8, mockUser, mockCourse);
        mockRating.setAuthor(new User());
        mockRating.setCourse(new Course());
        mockRating.setRating(3);
        ratingService.create(mockRating);
        Mockito.verify(mockRatingRepository, Mockito.times(1)).create(mockRating);
    }

    @Test
    public void update_Should_Call_Repository_Update() {
        Rating mockRating = new Rating(90, mockUser, mockCourse);
        ratingService.update(1, mockRating);
        Mockito.verify(mockRatingRepository, Mockito.times(1)).update(mockRating);
    }

    @Test
    public void update_Should_Call_Repository_Delete() {
        Rating mockRating = new Rating(90, mockUser, mockCourse);
        Mockito.when(mockRatingRepository.getRatingById(1))
                .thenReturn(mockRating);
        ratingService.delete(1);
        Mockito.verify(mockRatingRepository, Mockito.times(1)).delete(mockRating);
    }
}
