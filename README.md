# Virtual Teacher

Virtual Teacher is an online learning platform where teachers can create courses with lectures and assignments and students can complete them. Similar to Udacity or Udemy.

## Features

### Non-registered users:

- Can browse all the topics and courses on the home page
- Can login to the application

### Students:

- Can browse and enroll to a particular course
- Can download lecture's assignment
- Can upload a solution to a given lecture
- Can rate the course after course completion
- Can view their profile details and change their password
- Can view their enrolled and completed courses
- Can logout from the application

### Teachers:

- Can create courses
- Can add lectures and upload assignments and videos to the course
- Can assess student solutions and give grades to lectures
- Can view their profile details and change their password
- Can view their draft and ready courses

### Admin:

- Can change a student to teacher
- Can edit and delete other users


## Endpoints

### CourseMVC

- GET /courses/:id/ : view a course with its lectures
- GET /courses/:id/edit/ : view edit course form
- POST /courses/:id/edit/ : edit a course
- GET /courses/new : view new course form
- POST /courses/new : create a new course
- POST /courses/:id/lectures/new: create a lecture and add to a course

### UserMVC

- GET /users/:id : view a user with their profile details
- GET /users/:id/edit : view edit user form
- PUT /users/:id/edit : edit a user
- GET /users/new : view new user form
- POST /users/new : create a new user
- GET /users/:id/password : view edit password form
- POST /users/:id/password : edit user password
- DELETE /users/:id/delete : delete a user
- GET /users : list all paginated users
- POST /ratings/:courseid : give a rating to the course as a user
- POST /uploaded-solutions/assignments/{assignmentId} : upload a solution to assignment as a user
- GET /solutions : list all user solutions
- POST /solutions/assessments/:solutionid : give an assessment to a soluton as a teacher

### UserCourseMVC
- GET /users/:id/courses : view all user courses

### LectureMVC
- GET /lectures/:id : view a lecture

### HomeMVC
- GET /home : view homepage
- GET / : redirect to /home
- GET /register : view register form
- POST /register : create a new user
- GET /login : view login form

## Testing
### Line coverage
```
100% classes 70% lines
```
![Alt text](documentation/coverage.png?raw=true "Test coverage")


## Database

### Schema
```
/sqlmodels/learningsys-schema.sql
```
### Diagram
![Alt text](documentation/db-diagram.png?raw=true "Database diagram")

## Trello
![Alt text](documentation/trello.png)

## Samples
![Alt text](documentation/homepage.png)
![Alt text](documentation/course.png)
![Alt text](documentation/profile.png)
![Alt text](documentation/teachercourses.png)
![Alt text](documentation/solutions.png)
![Alt text](documentation/usercourses.png)
![Alt text](documentation/usercourse.png)

## Authors
Kristina Kostova and Dimitar Kochankov